
#include <common/decoding/OfflineDecoderHandler.h>


extern "C" {
    void* create_hlt_event(void* subevent, uint_fast32_t timestamp, uint_fast8_t finetime) {
        na62::EventIndexer* event = new na62::EventIndexer(static_cast<na62::l0::Subevent*>(subevent), timestamp);
        event->setFinetime(finetime);
        na62::DecoderHandler* decoded_event = new na62::DecoderHandler(event);
        return static_cast<void*>(decoded_event);
    }

    void delete_hlt_event(void* decoded_event) {
        delete (static_cast<na62::DecoderHandler*>(decoded_event))->getDecodedEvent();
        delete static_cast<na62::DecoderHandler*>(decoded_event);
    }
}

