/*
 * LAVAlgo.cpp
 *
 *  Created on: 7 Sep 2015
 *      Author: romano
 */
#include "LAVAlgo.h"

#include <string.h>
#include <math.h>
#include <sys/time.h>

#ifndef ONLINEHLT
#include <l0/Subevent.h>
#else
#include <l0/offline/Subevent.h>
#endif
#include <options/Logging.h>

#include <common/decoding/TrbFragmentDecoder.h>
#include <l1/L1InfoToStorage.h>
#include <l1/StrawAlgo.h>
#include <l1/straw_algorithm/Track.h>

#include <iostream>

extern "C" {
    void initialize_lav() {
        l1LAV l1LAVStruct;
        l1LAVStruct.configParams.l1TrigProcessID = 2;
        l1LAVStruct.configParams.l1TrigMaskID = 0;
        l1LAVStruct.configParams.l1TrigEnable = 1;
        l1LAVStruct.configParams.l1TrigLogic = 1;
        l1LAVStruct.configParams.l1TrigFlag = 0;
        l1LAVStruct.configParams.l1TrigRefTimeSourceID = 0;
	l1LAVStruct.configParams.l1TrigOnlineTimeWindow = 6;

        na62::LAVAlgo::initialize(0, l1LAVStruct);
    }

    void load_lav_conf(std::vector<std::string>* vec_of_strings) {
      na62::LAVAlgo::loadConfigurationFile(vec_of_strings->at(0), vec_of_strings->at(1));
    }

    uint_fast8_t process_lav_nostraw(uint l0MaskID, void* decoder_ptr, void* l1_info_ptr) {
        na62::DecoderHandler* decoded_event = reinterpret_cast<na62::DecoderHandler*>(decoder_ptr);
        L1InfoToStorage* l1_info = reinterpret_cast<L1InfoToStorage*>(l1_info_ptr);
        return na62::LAVAlgo::processLAVTrigger(l0MaskID, *decoded_event, l1_info);
    }

    uint_fast8_t process_lav(uint l0MaskID, void* decoder_ptr, void* l1_info_ptr, void* straw_algo_ptr) {
        na62::DecoderHandler* decoded_event = reinterpret_cast<na62::DecoderHandler*>(decoder_ptr);
        L1InfoToStorage* l1_info = reinterpret_cast<L1InfoToStorage*>(l1_info_ptr);
	na62::StrawAlgo* straw_algo = reinterpret_cast<na62::StrawAlgo*>(straw_algo_ptr);
        return na62::LAVAlgo::processLAVSTRAWTrigger(l0MaskID, *decoded_event, l1_info, straw_algo);
    }
}

namespace na62 {

  uint LAVAlgo::AlgoID_; //0 for CHOD, 1 for RICH, 2 for KTAG, 3 for LAV, 4 for IRCSAC, 5 for Straw, 6 for MUV3, 7 for NewCHOD
  uint LAVAlgo::AlgoLogic_[16];
  uint LAVAlgo::AlgoRefTimeSourceID_[16];
  double LAVAlgo::AlgoOnlineTimeWindow_[16];

  LAVParsConfFile* LAVAlgo::InfoLAV_ = LAVParsConfFile::GetInstance();
  int* LAVAlgo::StationZPos_ = nullptr;
  float* LAVAlgo::thresholdVals_ = nullptr;
  float* LAVAlgo::slewingParams_ = nullptr;
  float slewParam_ToT[5] = {7.04125e+00, 2.07619e-01, -4.12831e-02, 1.18392e-03, -9.57030e-06};

  void LAVAlgo::initialize(uint i, l1LAV &l1LAVStruct) {

    AlgoID_ = l1LAVStruct.configParams.l1TrigMaskID;
    AlgoLogic_[i] = l1LAVStruct.configParams.l1TrigLogic;
    AlgoRefTimeSourceID_[i] = l1LAVStruct.configParams.l1TrigRefTimeSourceID; //0 for L0TP, 1 for CHOD, 2 for RICH
    AlgoOnlineTimeWindow_[i] = l1LAVStruct.configParams.l1TrigOnlineTimeWindow;
}

  void LAVAlgo::loadConfigurationFile(std::string absolute_chMapFile_path, std::string absolute_slewing_path) {
	InfoLAV_->loadConfigFile(absolute_chMapFile_path);
	StationZPos_ = InfoLAV_->getLAVStationZPosition();

	InfoLAV_->readSlewingParams(absolute_slewing_path);
	thresholdVals_ = InfoLAV_->getLAVThresholds();
	slewingParams_ = InfoLAV_->getLAVSlewingParams();
}

uint_fast8_t LAVAlgo::processLAVTrigger(uint l0MaskID, DecoderHandler& decoder, L1InfoToStorage* l1Info) {

	using namespace l0;

//	LOG_INFO("Initial Time " << time[0].tv_sec << " " << time[0].tv_usec);
//	LOG_INFO("LAV: event timestamp = " << std::hex << decoder.getDecodedEvent()->getTimestamp() << std::dec);
//	LOG_INFO("LAV: event reference fine time from L0TP " << std::hex << (uint)decoder.getDecodedEvent()->getFinetime() << std::dec);

	/*
	 * TODO: The same logic needs to be developed for RICHRefTime
	 */
	double refTimeL0TP = 0.;
	double averageCHODHitTime = 0.;
	bool isCHODRefTime = false;
	if (!AlgoRefTimeSourceID_[l0MaskID]) {
		refTimeL0TP = decoder.getDecodedEvent()->getFinetime() * 0.097464731802;
//		LOG_INFO("L1 reference finetime from L0TP (ns) " << refTimeL0TP);
	} else if (AlgoRefTimeSourceID_[l0MaskID] == 1) {
		if (l1Info->isL1CHODProcessed()) {
			isCHODRefTime = 1;
			averageCHODHitTime = l1Info->getCHODAverageTime();
		} else
			LOG_ERROR("LAVAlgo.cpp: Not able to use averageCHODHitTime as Reference Time even if requested!");
	} else
		LOG_ERROR("LAVAlgo.cpp: L1 Reference Time Source ID not recognised !!");

	uint nHits = 0;
	uint nEdgesTot = 0;

	//TODO: chkmax need to be USED
	DecoderRange<TrbFragmentDecoder> x = decoder.getLAVDecoderRange();
	if (x.begin() == x.end()) {
		LOG_ERROR("LAV: Empty decoder range!");
		l1Info->setL1LAVBadData();
		return 0;
	}

	int hit[maxNROchs] = { 0 };

	for (TrbFragmentDecoder* lavPacket : decoder.getLAVDecoderRange()) {
//	LOG_INFO("First time check (inside iterator) " << time[1].tv_sec << " " << time[1].tv_usec);

		if((lavPacket->getSourceSubId() == 0) || (lavPacket->getSourceSubId() == 11)) continue;

		if (!lavPacket->isReady() || lavPacket->isBadFragment()) {
			LOG_ERROR("LAV: This looks like a Bad Packet!!!! ");
			l1Info->setL1LAVBadData();
			return 0;
		}

		for (uint i = 0; i != maxNROchs; i++) {
			hit[i] = 0;
		}

		/**
		 * Get Arrays with hit Info
		 */
		const uint64_t* const edgeTime = lavPacket->getTimes();
		const uint_fast8_t* const edgeChID = lavPacket->getChIDs();
		const bool* const edgeIsLeading = lavPacket->getIsLeadings();
		const uint_fast8_t* const edgeTdcID = lavPacket->getTdcIDs();
		double time, dtL0TP, dtCHOD;

		uint numberOfEdgesOfCurrentBoard = lavPacket->getNumberOfEdgesStored();

//		LOG_INFO("LAV: Fragment ID " << lavPacket->getFragmentNumber() << " Tel62 ID " << lavPacket->getSourceSubId() << " - Number of Edges found " << numberOfEdgesOfCurrentBoard);

		for (uint iEdge = 0; iEdge != numberOfEdgesOfCurrentBoard; iEdge++) {
//			LOG_INFO("Edge " << iEdge << " ID " << edgeIsLeading[iEdge]);
//			LOG_INFO("Edge " << iEdge << " chID " << (uint) edgeChID[iEdge]);
//			LOG_INFO("Edge " << iEdge << " tdcID " << (uint) edgeTdcID[iEdge]);
//			LOG_INFO("Edge " << iEdge << " time " << std::hex << edgeTime[iEdge] << std::dec);

			const int roChIDPerTrb = edgeTdcID[iEdge] * 32 + edgeChID[iEdge];
//			LOG_INFO("LAV Algo: Found R0chID " << roChIDPerTrb);

			if ((roChIDPerTrb & 1) == 0) {
				if (edgeIsLeading[iEdge]) {
					time = (edgeTime[iEdge] - decoder.getDecodedEvent()->getTimestamp() * 256.) * 0.097464731802;
//					LOG_INFO("Edge time (ns) " << time);

					if (!isCHODRefTime) {
						dtL0TP = fabs(time - refTimeL0TP);
						dtCHOD = -1.0e+28;
					} else
						dtCHOD = fabs(time - averageCHODHitTime);
//					LOG_INFO("Is Low Threshold:  " << ((roChIDPerTrb & 1) == 0) << " dtL0TP " << dtL0TP << " dtCHOD " << dtCHOD);

					if ((!isCHODRefTime && dtL0TP < AlgoOnlineTimeWindow_[l0MaskID])
							|| (isCHODRefTime && dtCHOD < AlgoOnlineTimeWindow_[l0MaskID])) {
						hit[roChIDPerTrb]++;
//						LOG_INFO("Increment hit[" << roChIDPerTrb << "] to " << hit[roChIDPerTrb]);
					}
				} else if (hit[roChIDPerTrb]) {
					nHits++;
//					LOG_INFO("Increment nHits " << nHits);

					if (nHits >= 3) {
						l1Info->setL1LAVNHits(nHits);
						l1Info->setL1LAVProcessed();
//						LOG_INFO("LAVAlgo: Returning nHits >= 3 !!!!");
						return 1;
					}
				}
			} else {
//				printf("ODD! - Soglia alta! \n");
			}
		}
//	LOG_INFO("time check " << time[2].tv_sec << " " << time[2].tv_usec);
		nEdgesTot += numberOfEdgesOfCurrentBoard;
	}

	if (!nEdgesTot)
		l1Info->setL1LAVEmptyPacket();

//	LOG_INFO("LAV: Analysing Event " << decoder.getDecodedEvent()->getEventNumber());
//	LOG_INFO("Timestamp " << std::hex << decoder.getDecodedEvent()->getTimestamp() << std::dec);
//	LOG_INFO("time check (final)" << time[3].tv_sec << " " << time[3].tv_usec);

//	for (int i = 0; i < 3; i++) {
//		if (i)
//			LOG_INFO(((time[i+1].tv_sec - time[i].tv_sec)*1e6 + time[i+1].tv_usec) - time[i].tv_usec << " ");
//	}
//	LOG_INFO(((time[3].tv_sec - time[0].tv_sec)*1e6 + time[3].tv_usec) - time[0].tv_usec);

//	LOG_INFO("nEdges_tot " << nEdgesTot << " - nHits " << nHits);

	l1Info->setL1LAVNHits(nHits);
	l1Info->setL1LAVProcessed();
	return (nHits >= 3);
}


  uint_fast8_t LAVAlgo::processLAVSTRAWTrigger(uint l0MaskID, DecoderHandler& decoder, L1InfoToStorage* l1Info, StrawAlgo *runstraw) {

	using namespace l0;

	double refTimeL0TP = 0.;
	double averageCHODHitTime = 0.;
	bool isCHODRefTime = false;
	if (!AlgoRefTimeSourceID_[l0MaskID]) {
		refTimeL0TP = decoder.getDecodedEvent()->getFinetime() * 0.097464731802;
//		LOG_INFO("L1 reference finetime from L0TP (ns) " << refTimeL0TP);
	} else if (AlgoRefTimeSourceID_[l0MaskID] == 1) {
		if (l1Info->isL1CHODProcessed()) {
			isCHODRefTime = 1;
			averageCHODHitTime = l1Info->getCHODAverageTime();
		} else
			LOG_ERROR("LAVAlgo.cpp: Not able to use averageCHODHitTime as Reference Time even if requested!");
	} else
		LOG_ERROR("LAVAlgo.cpp: L1 Reference Time Source ID not recognised !!");


	// if LAV event is empty, stop processing and return 0 (event accepted)
	DecoderRange<TrbFragmentDecoder> x = decoder.getLAVDecoderRange();
	if (x.begin() == x.end()) {
	  LOG_ERROR("LAV: Empty decoder range!");
	  l1Info->setL1LAVBadData();
	  return 0;
	}

	// most downstream vertex. When LAV algo runs before STRAW,
	// this remains 0.0 and there is no cut on the station Z position
	float Zvertex = 0;

	// this part is active only when the LAV algorithm is running after the STRAW
	if(l1Info->isL1StrawProcessed()) {
	  
	  // event accepted in case STRAW event is 'overflow' or 'bad data'
	  if( l1Info->isL1StrawOverflow() || l1Info->isL1StrawBadData() ){
	    l1Info->setL1LAVProcessed();
	    return 0; 
	  }
	  
	  // count number of pinunu-like tracks (good tracks),
	  // and get the most downstream vertex.
	  uint nGoodSTRAWTracks = 0;
	  for (uint iTrk = 0; iTrk < l1Info->getL1StrawNTracks(); ++iTrk) {
	    
	    na62::Track &STRAWTrack = runstraw->getTracks(iTrk);
	    float Momentum = 0.001*STRAWTrack.pz; // GeV
	    if(Momentum > (0.001*l1Info->getL1StrawPNNMomentumCut())) {
	      continue;
	    }
	    nGoodSTRAWTracks++;
	    float Zvertex_track = STRAWTrack.zvertex; // mm
	    if(Zvertex_track > Zvertex) Zvertex = Zvertex_track;

	  }

	  // If there are no good tracks, reject the event. 
	  // Note this code is only running if the LAV algo is run after the STRAW
	  if(nGoodSTRAWTracks==0){
	    l1Info->setL1LAVProcessed();
	    return 1;
	  }
	  
	}

	float dVleads = thresholdVals_[1] - thresholdVals_[0];
	std::vector<float> CorTime;
	CorTime.reserve(70); //usually no more than 35 hits

	for (TrbFragmentDecoder* lavPacket : decoder.getLAVDecoderRange()) {

	  // accept the event if there is a problem with the LAV data
	  if (!lavPacket->isReady() or lavPacket->isBadFragment()) {
	    LOG_ERROR("LAV: This looks like a Bad Packet!!!! ");
	    l1Info->setL1LAVBadData();
	    return 0;
	  }

	  // ignore lav stations 1 and 12
	  if((lavPacket->getSourceSubId() == 0) or (lavPacket->getSourceSubId() == 11)) {
	    continue;
	  }

	  // if the station is upstream of the most downstream vertex, the station is ignored.
	  // When the LAV algo runs before STRAW, this cut does nothing as Zvertex == 0
	  if((1000*StationZPos_[lavPacket->getSourceSubId()]) < Zvertex) {
	    continue;
	  }

	  uint64_t lowleadtime[256] = {0};
	  
	  const uint64_t* const edgeTime = lavPacket->getTimes();
          const uint_fast8_t* const edgeChID = lavPacket->getChIDs();
          const bool* const edgeIsLeading = lavPacket->getIsLeadings();
          const uint_fast8_t* const edgeTdcID = lavPacket->getTdcIDs();
	  uint numberOfEdgesOfCurrentBoard = lavPacket->getNumberOfEdgesStored();

	  for (uint iEdge = 0; iEdge != numberOfEdgesOfCurrentBoard; iEdge++) {

	    const int roChIDPerTrb = edgeTdcID[iEdge] * 32 + edgeChID[iEdge];
            const int blockID = roChIDPerTrb/2;

	    if (edgeIsLeading[iEdge]) { // is leading
              if ((roChIDPerTrb % 2) == 0) { //is low
                lowleadtime[blockID] = edgeTime[iEdge];
              }
	      else if(((roChIDPerTrb % 2) == 1) && (lowleadtime[blockID]>0) ){ //is high && there was a leading low
		// EdgeMasks considered: 3-7-11-15
                float lowleadft = (lowleadtime[blockID] - (decoder.getDecodedEvent()->getTimestamp() * 256.)) * 0.097464731802;
                float highleadft = (edgeTime[iEdge] - (decoder.getDecodedEvent()->getTimestamp() * 256.)) * 0.097464731802;
                float dTleads = highleadft - lowleadft;
                if(fabs(dTleads) < 15.){ //RiseTimeSafetyMargin
                  //residual slewing
                  float dTleadeff = dTleads;
                  if(dTleads < 0.2) dTleadeff = 0.2;
                  if(dTleads > 10.) dTleadeff = 10.;
                  float x = dTleadeff/4. - 1.;
                  float residual = 0;
                  for(int i = 0; i < 5; i++){
                    residual += slewingParams_[i] * pow(x, i);
                  }
                  //corrected time:
                  CorTime.push_back(lowleadft - thresholdVals_[0]/dVleads * dTleads - residual);
		  // reset leading low time to zero
		  lowleadtime[blockID]=0;
                }
	      }
	    } else { // is trailing
              if(((roChIDPerTrb % 2) == 0) && (lowleadtime[blockID]>0) ){
                // is low && there was a leading low and not a leading high
                // EdgeMasks considered: 9 
                float lowleadft = (lowleadtime[blockID] - (decoder.getDecodedEvent()->getTimestamp() * 256.)) * 0.097464731802;
                float lowttrailft = (edgeTime[iEdge] - (decoder.getDecodedEvent()->getTimestamp() * 256.)) * 0.097464731802;
                float ToT = lowttrailft - lowleadft;
                CorTime.push_back(lowleadft-slewParam_ToT[0]-slewParam_ToT[1]*ToT-slewParam_ToT[2]*ToT*ToT
                                  -slewParam_ToT[3]*ToT*ToT*ToT-slewParam_ToT[4]*ToT*ToT*ToT*ToT);
		// reset leading low time to zero
		lowleadtime[blockID]=0;
              }
            }
	  }
	}

	// get reference time
	float ref=0.0; 
	if (!isCHODRefTime) {
	  ref = refTimeL0TP;
	}
	else{
	  ref = averageCHODHitTime;
	}

	float dt=0.0;
	uint nLAVHitsDownstream=0;
	uint_fast8_t LAVTrkMatching = 0;
	for (auto & time: CorTime) {

	  dt = fabs(time - ref);

	  if (dt < AlgoOnlineTimeWindow_[l0MaskID]){
	    nLAVHitsDownstream++;
	  }

	  if (nLAVHitsDownstream >= 3) {
	    //LOG_INFO("BREAKING because nHits for track " << iTrk << " >= 3 " << nLAVHitsPerTrack[iTrk]);
	    LAVTrkMatching = 1;
	    break;
	  }
	}

	//LOG_INFO("Number of Hits for track " << iTrk << " = " << nLAVHitsPerTrack[iTrk]);

	//if(nGoodSTRAWTracks){
	//LAVTrkMatching = (nTracksWithLAVHitsMatching<nGoodSTRAWTracks)? 0 : 1;
	//} //else LOG_ERROR("LAV: No Good Tracks from the Spectrometer!!!! ");

	l1Info->setL1LAVNHits(nLAVHitsDownstream);
	l1Info->setL1LAVProcessed();
	return (LAVTrkMatching);
}



void LAVAlgo::writeData(L1Algo* algoPacket, uint l0MaskID, L1InfoToStorage* l1Info) {

	if (AlgoID_ != algoPacket->algoID)
		LOG_ERROR("LAVAlgo.cpp: Algo ID does not match with Algo ID already written within the packet!");

	algoPacket->algoID = AlgoID_;
	algoPacket->onlineTimeWindow = (uint) AlgoOnlineTimeWindow_[l0MaskID];
//	algoPacket->qualityFlags = (l1Info->isL1LAVProcessed() << 6) | (l1Info->isL1LAVEmptyPacket() << 4) | (l1Info->isL1LAVBadData() << 2) | AlgoRefTimeSourceID_[l0MaskID];
	algoPacket->qualityFlags = (AlgoRefTimeSourceID_[l0MaskID] << 7) | (l1Info->isL1LAVProcessed() << 6) | (l1Info->isL1LAVEmptyPacket() << 4) | (l1Info->isL1LAVBadData() << 2)
		| ((uint) l1Info->getL1LAVTrgWrd(l0MaskID));

	algoPacket->l1Data[0] = l1Info->getL1LAVNHits();
	if (!AlgoRefTimeSourceID_[l0MaskID]) {
		algoPacket->l1Data[1] = l1Info->getL1RefTimeL0TP();
	} else if (AlgoRefTimeSourceID_[l0MaskID] == 1) {
		algoPacket->l1Data[1] = l1Info->getCHODAverageTime(); //this is a double!!!
	} else
		LOG_ERROR("LAVAlgo.cpp: L1 Reference Time Source ID not recognised !!");

	algoPacket->numberOfWords = (sizeof(L1Algo) / 4.);
//	LOG_INFO("l0MaskID " << l0MaskID);
//	LOG_INFO("algoID " << (uint)algoPacket->algoID);
//	LOG_INFO("quality Flags " << (uint)algoPacket->qualityFlags);
//	LOG_INFO("online TW " << (uint)algoPacket->onlineTimeWindow);
//	LOG_INFO("Data Words " << algoPacket->l1Data[0] << " " << algoPacket->l1Data[1]);
}

}
/* namespace na62 */
