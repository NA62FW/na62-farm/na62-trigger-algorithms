/*
 * ParseConfFile.cpp
 *
 *  Created on: 20 Apr 2015
 *      Author: vfascian
 *  Modified on: 11 May 2015
 *      Author: romano
 */


#include <string>

#include <options/Logging.h>
#include <common/ConfFileReader.h>
#include <l1/ConfPath.h>
#include "ParsConfFile.h"

MUV3ParsConfFile* MUV3ParsConfFile::theInstance = nullptr;

MUV3ParsConfFile::MUV3ParsConfFile() {
}

void MUV3ParsConfFile::loadConfigFile(std::string absolute_file_path) {
	ConfFileReader fileName_(absolute_file_path);

	if (fileName_.isValid()) {
	  LOG_INFO(" > HLT MUV3 configuration file opening: " << fileName_.getFilename());

		while (fileName_.nextLine()) {
		  if (fileName_.getField<std::string>(1) == "#") {
				continue;
			}
		  if (fileName_.getField<std::string>(1) == "NROChannels=") {
				nroChannels = fileName_.getField<int>(2);
//				LOG_INFO("nroChannels " << nroChannels);
			}
		  if (fileName_.getField<std::string>(1).find("ChRemap_")
		                  != std::string::npos) {
		          for (int iCh = 0; iCh < nroChannels / 16; ++iCh) {
			          char name[1000];
				  sprintf(name, "ChRemap_%04d=", iCh);
				  std::string remap = (std::string) name;
//				  LOG_INFO(remap);
//				  LOG_INFO(fileName_.getField<string>(1));

				  if (fileName_.getField<std::string>(1) == remap) {
						for (int jCh = 0; jCh < 16; jCh++) {
							geoPMTMap[iCh * 16 + jCh] =
									fileName_.getField<int>(jCh + 2);
//							LOG_INFO("geoPMTMap= " << geoPMTMap[iCh * 16 + jCh]);
						}
					}
				}
			}
		}
	} else {
	        LOG_ERROR(" > HLT MUV3 Configuration file not found: " << fileName_.getFilename());
	}
}

MUV3ParsConfFile::~MUV3ParsConfFile() {
}

MUV3ParsConfFile* MUV3ParsConfFile::GetInstance() {

	if (theInstance == nullptr) {
		theInstance = new MUV3ParsConfFile();
	}
	return theInstance;

}

int* MUV3ParsConfFile::getGeoPMTMap() {
	return geoPMTMap;
}
