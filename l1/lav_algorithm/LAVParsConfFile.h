/*
 * ParsConfFile.h
 *
 *  Created on: 7 Sep 2015
 *      Author: romano
 *
 *  Modified on: April 2022
 *      Author: romano
 *      Email: angela.romano@cern.ch
 */

#ifndef L1_LAV_ALGORITHM_LAVPARSCONFFILE_H_
#define L1_LAV_ALGORITHM_LAVPARSCONFFILE_H_

class LAVParsConfFile {
public:
       LAVParsConfFile();
       void loadConfigFile(std::string absolute_file_path);

       ~LAVParsConfFile();

       static LAVParsConfFile* GetInstance();

       int* getGeoLGMap();
       int* getLAVStationZPosition();

       void readSlewingParams(std::string absolute_slewing_path);
       float* getLAVThresholds();
       float* getLAVSlewingParams();

private:

       static LAVParsConfFile* theInstance;  // singleton instance

       int nroChannels;
       int geoLGMap[6144];
       int lavStationZFrontPosition[12];

       float lavThresholds[2];
       float lavSlewingParams[5];
};

#endif /* L1_LAV_ALGORITHM_LAVPARSCONFFILE_H_ */
