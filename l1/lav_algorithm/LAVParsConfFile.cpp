/*
 * ParsConfFile.cpp
 *
 *  Created on: 7 Sep 2015
 *      Author: romano
 */


#include <string>

#include <options/Logging.h>
#include <common/ConfFileReader.h>
#include <l1/ConfPath.h>
#include "LAVParsConfFile.h"

LAVParsConfFile* LAVParsConfFile::theInstance = nullptr;

LAVParsConfFile::LAVParsConfFile() {
}

void LAVParsConfFile::loadConfigFile(std::string absolute_file_path) {
	ConfFileReader fileName_(absolute_file_path);

	if (fileName_.isValid()) {
		LOG_INFO(" > HLT LAV configuration file opening: " << fileName_.getFilename());

		while (fileName_.nextLine()) {
			if (fileName_.getField<std::string>(1) == "#") {
				continue;
			}
			if (fileName_.getField<std::string>(1) == "NROChannels=") {
				nroChannels = fileName_.getField<int>(2);
//				LOG_INFO("nroChannels " << nroChannels);
			}
			if (fileName_.getField<std::string>(1).find("ChRemap_")
					!= std::string::npos) {
				for (int iCh = 0; iCh < nroChannels / 16; ++iCh) {
					char name[1000];
					sprintf(name, "ChRemap_%04d=", iCh);
					std::string remap = (std::string) name;
//					LOG_INFO(remap);
//					LOG_INFO(fileName_.getField<string>(1));

					if (fileName_.getField<std::string>(1) == remap) {
						for (int jCh = 0; jCh < 16; jCh++) {
							geoLGMap[iCh * 16 + jCh] =
									fileName_.getField<int>(jCh + 2);
//							LOG_INFO("geoLGMap " << geoLGMap[iCh * 16 + jCh]);
						}
					}
				}
			}
		}
	} else {
		LOG_ERROR(" > HLT LAV Configuration file not found: " << fileName_.getFilename());
	}

}

LAVParsConfFile::~LAVParsConfFile() {
}

LAVParsConfFile* LAVParsConfFile::GetInstance() {

	if (theInstance == nullptr) {
		theInstance = new LAVParsConfFile();
	}
	return theInstance;

}

int* LAVParsConfFile::getGeoLGMap() {
	return geoLGMap;
}

int* LAVParsConfFile::getLAVStationZPosition(){
  
	lavStationZFrontPosition[0]  = 120.595; // m
	lavStationZFrontPosition[1]  = 128.205; // m
	lavStationZFrontPosition[2]  = 135.815; // m
	lavStationZFrontPosition[3]  = 143.425; // m
	lavStationZFrontPosition[4]  = 151.035; // m
	lavStationZFrontPosition[5]  = 164.545; // m
	lavStationZFrontPosition[6]  = 172.055; // m
	lavStationZFrontPosition[7]  = 179.565; // m
	lavStationZFrontPosition[8]  = 191.941; // m
	lavStationZFrontPosition[9]  = 202.334; // m
	lavStationZFrontPosition[10] = 216.760; // m
	lavStationZFrontPosition[11] = 238.150; // m      
	return lavStationZFrontPosition;

}

////////////////////////////////////////////////////
// Read SLEWING Correction Parameters from LAV.Conf

void LAVParsConfFile::readSlewingParams(std::string absolute_slewing_path) {

  ConfFileReader fileName_(absolute_slewing_path);

  if (fileName_.isValid()) {
    LOG_INFO(" > LAV.conf slewing correction file opening: " << fileName_.getFilename());

    while (fileName_.nextLine()) {

      if (fileName_.getField<std::string>(1) == "#") {
	continue;
      }
      if (fileName_.getField<std::string>(1) == "ThresholdNominalValues") {
	lavThresholds[0] = fileName_.getField<float>(2);
	lavThresholds[1] = fileName_.getField<float>(3);
	LOG_INFO("LAV Thresholds " << lavThresholds[0] << " " << lavThresholds[1]);
      }
      if (fileName_.getField<std::string>(1) == "LAVSlewingCorrectionParameters") {
	lavSlewingParams[0] = fileName_.getField<float>(6);
	lavSlewingParams[1] = fileName_.getField<float>(7);
	lavSlewingParams[2] = fileName_.getField<float>(8);
	lavSlewingParams[3] = fileName_.getField<float>(9);
	lavSlewingParams[4] = fileName_.getField<float>(10);
	LOG_INFO("LAV Slewing Parameters " 
		 << lavSlewingParams[0] << " " 
		 << lavSlewingParams[1] << " " 
		 << lavSlewingParams[2] << " " 
		 << lavSlewingParams[3] << " " 
		 << lavSlewingParams[4]);
      }
    }
  }  else {
    LOG_ERROR(" > HLT LAV.conf slewing correction file not found: " << fileName_.getFilename());
	}
}

float* LAVParsConfFile::getLAVThresholds() {
	return lavThresholds;
}

float* LAVParsConfFile::getLAVSlewingParams() {
	return lavSlewingParams;
}
