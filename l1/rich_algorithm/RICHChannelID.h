/*
 * RICHChannelID.h
 *
 *  Created on: 21 Apr 2015
 *      Author: vfascian
 */

#ifndef L1_RICH_ALGORITHM_RICHCHANNELID_H_
#define L1_RICH_ALGORITHM_RICHCHANNELID_H_

class RICHChannelID{

public:
	RICHChannelID(int geoID);
	~RICHChannelID();

	int getDiskID();
	int getUpDownDiskID();
	int getSuperCellID();
	int getOrSuperCellID();
	int getPmtID();
	int getChannelSeqID();


private:
	int diskID;
	int upDownDiskID;
	int superCellID;
	int orSuperCellID;
	int pmtID;
};




#endif /* L1_RICH_ALGORITHM_RICHCHANNELID_H_ */
