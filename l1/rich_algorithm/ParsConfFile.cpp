/*
 * ParsConfFile.cpp
 *
 *  Created on: 20 Apr 2015
 *      Author: vfascian
 *
 *  Modified on: 01 Oct 2021
 *      Author: romano
 *      Email: angela.romano@cern.ch
 */

#include "ParsConfFile.h"

#include <common/ConfFileReader.h>
#include <l1/ConfPath.h>

#include <options/Logging.h>
#include <iostream>


RICHParsConfFile* RICHParsConfFile::theInstance = nullptr;

RICHParsConfFile::RICHParsConfFile() {

}


RICHParsConfFile::~RICHParsConfFile() {
}

RICHParsConfFile* RICHParsConfFile::GetInstance() {

	if (theInstance == nullptr) {
		theInstance = new RICHParsConfFile();
	}
	return theInstance;

}

void RICHParsConfFile::loadConfigFile(std::string absolute_file_path) {
	ConfFileReader fileName_(absolute_file_path);

	if (fileName_.isValid()) {
		LOG_INFO(" > HLT RICH configuration file opening: " << fileName_.getFilename());

		while (fileName_.nextLine()) {

			if (fileName_.getField<std::string>(1) == "#") {
				continue;
			}
			if (fileName_.getField<std::string>(1) == "NROChannels=") {
				nroChannels = fileName_.getField<int>(2);
//				LOG_INFO("nChannels " << nChannels);
			}
			if (fileName_.getField<std::string>(1).find("ChRemap_") != std::string::npos) {
				for (int iCh = 0; iCh < nroChannels / 16; ++iCh) {
					char name[1000];
					sprintf(name, "ChRemap_%04d=", iCh);
					std::string remap = (std::string) name;
					//LOG_INFO(remap);
					//LOG_INFO(fileName.getField<string>(1));

					if (fileName_.getField<std::string>(1) == remap) {
						for (int jCh = 0; jCh < 16; jCh++) {
							geoPmsMap[iCh * 16 + jCh] = fileName_.getField<int>(jCh + 2);
//							LOG_INFO("geoPmsMap " << geoPmsMap[iCh * 16 + jCh]);
						}
					}
				}
			}
		}
	} else {
		LOG_ERROR(" > HLT RICH configuration file not found: " << fileName_.getFilename());
	}
}

int* RICHParsConfFile::getGeoPmsMap() {
	return geoPmsMap;
}

int RICHParsConfFile::getNROChannels() {
	return nroChannels;
}

void RICHParsConfFile::readPosPmsMap(std::string absolute_posPmsConf_path) {

	ConfFileReader fileName_(absolute_posPmsConf_path);

	if (fileName_.isValid()) {
		LOG_INFO(" > RICH PMT position and configuration file opening: " << fileName_.getFilename());

		while (fileName_.nextLine()) {

			if (fileName_.getField<std::string>(1) == "#") {
				continue;
			}
			if (fileName_.getField<std::string>(1) == "NChannels=") {
				nChannels = fileName_.getField<int>(2);
//				LOG_INFO("nChannels " << nChannels);
			}
			if (fileName_.getField<std::string>(1) == "JuraRotation=") {
				focalCenterJura[0] = fileName_.getField<int>(2);
				focalCenterJura[1] = fileName_.getField<int>(3);
//				LOG_INFO("Jura shift " << focalCenterJura[0] << " " << focalCenterJura[1]);
			}
			if (fileName_.getField<std::string>(1) == "SaleveRotation=") {
				focalCenterSaleve[0] = fileName_.getField<int>(2);
				focalCenterSaleve[1] = fileName_.getField<int>(3);
//				LOG_INFO("Saleve shift " << focalCenterSaleve[0] << " " << focalCenterSaleve[1]);
			}
			if (fileName_.getField<std::string>(1).find("PMPosition_SC_") != std::string::npos) {
				for (int iCh = 0; iCh < nChannels / 16; ++iCh) {
					char name[1000];
					sprintf(name, "PMPosition_SC_%d=", iCh);
					std::string position = (std::string) name;
					//LOG_INFO(remap);
					//LOG_INFO(fileName.getField<std::string>(1));

					if (fileName_.getField<std::string>(1) == position) {
						for (int jCh = 0; jCh < 16; ++jCh) {
							posPmsMap[iCh * 16 + jCh] = fileName_.getField<float>(jCh + 2);
//							LOG_INFO("posPmsMap " << posPmsMap[iCh * 16 + jCh]);
						}
					}
				}
			}

			if (fileName_.getField<std::string>(1).find("T0CorrectionFileInput") != std::string::npos) {
				fileT0 = fileName_.getField<std::string>(2);
//				LOG_INFO("fileT0 " << fileT0);
			}

			if (fileName_.getField<std::string>(1).find("MinPMsForEvent") != std::string::npos) {
				minPMsForEvent = fileName_.getField<int>(2);
//				LOG_INFO("minPMsForEvents" << minPMsForEvent);
			}

			if (fileName_.getField<std::string>(1).find("NCandidateClusteringIterations") != std::string::npos) {
				nCandClusteringIterations = fileName_.getField<int>(2);
//				LOG_INFO("nCandClusteringIterations " << nCandClusteringIterations);
			}
		}
	} else {
		LOG_ERROR(" > HLT RICH PMT and configuration file not found: " << fileName_.getFilename());
	}
}

float* RICHParsConfFile::getPosPmsMap() {
	return posPmsMap;
}

int* RICHParsConfFile::getFocalCenterJura() {
	return focalCenterJura;
}

int* RICHParsConfFile::getFocalCenterSaleve() {
	return focalCenterSaleve;
}

///////////////////////////////////
// Read mirror alignment parameters

void RICHParsConfFile::readMirrorAlignment(std::string absolute_mirrorAlign_path) {

  ConfFileReader fileName_(absolute_mirrorAlign_path);
  //std::string line;
  //int nLines = 0;

	if (fileName_.isValid()) {
		LOG_INFO(" > RICH Mirror Alignment configuration file opening: " << fileName_.getFilename());

		/*
		while (line.ReadLine(fileName_.getFilename())) {

		  if (line.BeginsWith("#")) continue;
		  TObjArray *l = line.Tokenize(" ");
		  if (nLines==0) {
		    extraOffsetJura[0]   = ((TObjString*)(l->At(0)))->GetString().Atof();
		    extraOffsetJura[1]   = ((TObjString*)(l->At(1)))->GetString().Atof();
		    extraOffsetSaleve[0] = ((TObjString*)(l->At(2)))->GetString().Atof();
		    extraOffsetSaleve[1] = ((TObjString*)(l->At(3)))->GetString().Atof();
		  }
		  else continue;
		  l->Delete();
		  delete l;
		  Nline++;
		}

		if (nLines!=1) {
		  cout << "[RICHParameters] Invalid input file format" << endl;
		}
		std::cout << "[RICHParameters] Mirror extra offsets: " <<
		  extraOffsetJura[0] << " " << extraOffsetJura[1] << " " << extraOffsetSaleve[0] << " " << extraOffsetSaleve[1] << std::endl;
		*/

		extraOffsetJura[0] = 19.79;
		extraOffsetJura[1] = 19.56;
		extraOffsetSaleve[0] = 20.23;
		extraOffsetSaleve[1] = 8.56;

	}  else {
		LOG_ERROR(" > HLT RICH Mirror Alignment configuration file not found: " << fileName_.getFilename());
	}
}

float* RICHParsConfFile::getExtraOffsetJura() {
	return extraOffsetJura;
}

float* RICHParsConfFile::getExtraOffsetSaleve() {
	return extraOffsetSaleve;
}

void RICHParsConfFile::readT0() {

//	LOG_INFO("RICH ParsFile::File T0 " << fileT0);

        std::string abs_file_path = "/workspace/na62-trigger-algorithms/l1/rich_algorithm/" + fileT0;
	ConfFileReader fileT0_(abs_file_path);

	if (not fileT0_.isValid()) {
		LOG_ERROR("T0 file not found");
		return;
        }

	LOG_INFO("Reading T0 file " << abs_file_path);

	while (fileT0_.nextLine()) {
		if (fileT0_.getField<std::string>(1) == "#") {
			continue;
		}

		if (fileT0_.getField<std::string>(1).find("T0_") != std::string::npos) {
			for (int iCh = 0; iCh < nChannels / 16; ++iCh) {
				char name[1000];
				sprintf(name, "T0_%d=", iCh);
				std::string time = (std::string) name;
				//LOG_INFO(remap);
				//LOG_INFO(fileName.getField<std::string>(1));

				if (fileT0_.getField<std::string>(1) == time) {
					for (int jCh = 0; jCh < 16; jCh++) {
						timeT0[iCh * 16 + jCh] = fileT0_.getField<float>(jCh + 2);
//						LOG_INFO("timeT0 " << timeT0[iCh * 16 + jCh]);
					}
				}
			}
		}
	}
}

float* RICHParsConfFile::getT0() {
	readT0();
	return timeT0;
}

int RICHParsConfFile::getMinPMsForEvent() {
	return minPMsForEvent;
}

int RICHParsConfFile::getNCandClusteringIterations() {
	return nCandClusteringIterations;
}
