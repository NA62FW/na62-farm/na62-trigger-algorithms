/*
 * ParsConfFile.h
 *
 *  Created on: 20 Apr 2015
 *      Author: vfascian
 *
 *  Modified on: 01 Oct 2021
 *      Author: romano
 *      Email: angela.romano@cern.ch
 */

#ifndef L1_RICH_ALGORITHM_PARSCONFFILE_H_
#define L1_RICH_ALGORITHM_PARSCONFFILE_H_

#include <string>

class RICHParsConfFile {
public:
	RICHParsConfFile();
	~RICHParsConfFile();

	static RICHParsConfFile* GetInstance();
	void loadConfigFile(std::string absolute_file_path);

	int* getGeoPmsMap();
	int getNROChannels();

	void readPosPmsMap(std::string absolute_posPmsConf_path);
	float* getPosPmsMap();
	int* getFocalCenterSaleve();
	int* getFocalCenterJura();

	void readMirrorAlignment(std::string absolute_mirrorAlign_path);
	float* getExtraOffsetSaleve();
	float* getExtraOffsetJura();

	void readT0();
	float* getT0();

	int getMinPMsForEvent();
	int getNCandClusteringIterations();

private:

	static RICHParsConfFile* theInstance;  // singleton instance

	int geoPmsMap[2560];
	int nChannels;
	int nroChannels;

	int focalCenterSaleve[2];
	int focalCenterJura[2];
	float posPmsMap[1952];

	float extraOffsetSaleve[2];
	float extraOffsetJura[2];

	float timeT0[1952];
	std::string fileT0;

	int minPMsForEvent;
	int nCandClusteringIterations;

};

#endif /* L1_RICH_ALGORITHM_PARSCONFFILE_H_ */
