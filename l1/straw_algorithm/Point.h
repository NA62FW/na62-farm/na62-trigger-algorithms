/*
 * Point.h
 *
 *  Created on: 7 Mar 2016
 *      Author: romano
 */

#ifndef L1_STRAW_ALGORITHM_POINT_H_
#define L1_STRAW_ALGORITHM_POINT_H_

namespace na62 {

class Point {
public:
	Point();
	Point(float a, float b, float c, float f, float e, float h, float i, int g, int d);
	virtual ~Point();
	void clear();

	float x;
	float y;
	float z;
	float trailing;
	float viewDistance;
	int nViews;	
	int views[4]; //elenco hit presenti nella casella centrale
	float errorx;
	float errory;
	int used;

	void setPoint(float a,float b,float c,float f,float e, float h, float i,int g,int d);
	void printPoint();
	void printPoint2 ();
	void clonePoint(Point p);

private:
};

} /* namespace na62 */

#endif /* L1_STRAW_ALGORITHM_POINT_H_ */
