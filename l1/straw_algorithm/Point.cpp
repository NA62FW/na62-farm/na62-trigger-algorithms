/*
 * Point.cpp
 *
 *  Created on: 7 Mar 2016
 *      Author: romano
 */

#include "Point.h"

#include <options/Logging.h>

namespace na62 {

Point::Point() {
	used = nViews = 0;
	x = y = z = trailing = viewDistance = 0.;
	views[0]=views[1]=views[2]=views[3]=-1;
	errorx = errory = -100.; 
}

Point::Point(float a, float b, float c, float f, float e, float h, float i, int g, int d)
{
	setPoint(a, b, c, f, e, h, i, g, d);
}

Point::~Point() {
	// TODO Auto-generated destructor stub
}

void Point::clear() {
	used = nViews = 0;
	x = y = z = trailing = viewDistance = 0.;
	views[0]=views[1]=views[2]=views[3]=-1;
	errorx = errory = -100.; 
}

void Point::setPoint(float a, float b, float c, float f, float e, float h, float i, int g, int d) {
	views[0]=views[1]=views[2]=views[3]=-1;
	z = a;
	x = b;
	y = c;
	trailing = f;
	viewDistance = e;
	nViews = g;
	used = d;
	errorx = h;
	errory = i;
}

void Point::printPoint() {
	printf("%f %f %f %lf %f %f %f %d %d\n", z, x, y, trailing, viewDistance, errorx, errory, nViews,
			used);
}

void Point::printPoint2() {
	printf("%f %f %f %lf %f %f %f %d\n", z, x, y, trailing, viewDistance, errorx, errory, nViews);
}

void Point::clonePoint(Point p) {
	z = p.z;
	x = p.x;
	y = p.y;
	trailing = p.trailing;
	viewDistance = p.viewDistance;
	used = p.used;
	nViews = p.nViews;
	errorx = p.errorx;
	errory = p.errory;

	views[0]=p.views[0];
	views[1]=p.views[1];
	views[2]=p.views[2];
	views[3]=p.views[3];
}
} /* namespace na62 */
