/*
 * Cluster.cpp
 *
 *  Created on: 3 Mar 2016
 *      Author: romano
 */

#include "Cluster.h"

#include <options/Logging.h>

namespace na62 {

Cluster::Cluster() {
	chamber = view = used = 0;
	coordinate = trailing = deltadistance = 0.;
	error = -100.;
}

Cluster::~Cluster() {
	// TODO Auto-generated destructor stub
}

void Cluster::setCluster(int a, int b, float c, float e, float f, float g, int d) {
	chamber = a;
	view = b;
	coordinate = c;
	used = d;
	trailing = e;
	deltadistance = f;
	error = g;
}

void Cluster::printCluster() {
	printf("%d %d %f %lf %f %f %d\n", chamber, view, coordinate, trailing,
			deltadistance, error, used);
}

void Cluster::printCluster2() {
	printf("%d %d %f %lf %f %f\n", chamber, view, coordinate, trailing,
			deltadistance, error);
}

} /* namespace na62 */
