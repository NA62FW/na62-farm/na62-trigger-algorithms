/*
 * StrawAlgo.cpp
 *
 *  Created on: Jun 23, 2021
 *      Author: Jacopo Pinzino
 *      Email: axr@hep.ph.bham.ac.uk 
 */

#include "StrawAlgo.h"

#include <sys/time.h>

#ifndef ONLINEHLT
#include <l0/Subevent.h>
#else
#include <l0/offline/Subevent.h>
#endif

#include <options/Logging.h>

#include "L1TriggerProcessor.h"

#include <common/decoding/SrbFragmentDecoder.h>
#include <l1/straw_algorithm/STRAWParsConfFile.h>
#include <l1/straw_algorithm/ChannelID.h>
#include <l1/straw_algorithm/DigiManager.h>

#define MAX_N_HIT_C 6
#define MAX_N_HIT_L 6
#define MAX_N_ADD_HIT 50

#define CLOCK_PERIOD 24.951059536
#define PASSO 0.0004
#define LMAGNET 3000
// #define ZMAGNET 197645 //old from Beatch file
#define ZMAGNET 196995 //new from reco

#define ZCHAMBER0 183526.5
#define ZCHAMBER1 194084.5
#define ZCHAMBER2 204477.5
#define ZCHAMBER3 218903.5

#define ZMUV3 246800.0
#define RMUV3MIN 90
#define RMUV3MAX 1400
#define RMUV3MINTIGHT 103
#define RMUV3MAXTIGHT 1320
#define MUV3GAPWIDTH 0.8

#define SIGMASLOPEH 0.1155
#define SIGMAPOINTH 4.33
#define SIGMAMULTSCATTERING 0.000942  //is to be divided for P(GeV) and multiplied for number of chamber
#define RESPOINT 0.177
#define debug 0  //1 normal debug, 2 point cout, 3 cluster cout, 4 errori punti, 5 leading e trailing hits, 6 punti per silvia
#define debtime 50

#define DZHVIEWS 57	//z distance between u and v or between x and y
#define DZTVIEWS 233	//z distance between v and x (the z order is u,v,x,y)
#define CHI2ERR 10 //empiric

#define MPION 139.57
#define MKAON 493.677



extern "C" {
    void initialize_straw() {
        l1Straw l1StrawStruct;
        l1StrawStruct.configParams.l1TrigProcessID = 3;
        l1StrawStruct.configParams.l1TrigMaskID = 0;
        l1StrawStruct.configParams.l1TrigEnable = 1;
        l1StrawStruct.configParams.l1TrigLogic = 1;
        l1StrawStruct.configParams.l1TrigFlag = 0;
        l1StrawStruct.configParams.l1TrigRefTimeSourceID = 0;
        l1StrawStruct.configParams.l1TrigOnlineTimeWindow = 5;
        l1StrawStruct.PNNMomentumCut = 60000.;

        na62::StrawAlgo::initialize(0, l1StrawStruct);
    }

    void load_straw_conf(std::vector<std::string>* vec_of_strings) {
      na62::StrawAlgo::loadConfigurationFile(vec_of_strings->at(0), 
					     vec_of_strings->at(1), 
					     vec_of_strings->at(2)); 
    }

    void* create_strawalgo() {
        return reinterpret_cast<void *>(new na62::StrawAlgo);
    }

    uint_fast8_t process_straw(uint l0MaskID, void* decoder_prt, void* l1_info_ptr, void* straw_algo_ptr) {
        na62::DecoderHandler* decoded_event = reinterpret_cast<na62::DecoderHandler*>(decoder_prt);
        L1InfoToStorage* l1_info = reinterpret_cast<L1InfoToStorage*>(l1_info_ptr);
        na62::StrawAlgo* straw_algo = reinterpret_cast<na62::StrawAlgo*>(straw_algo_ptr);
        return straw_algo->processStrawTrigger(l0MaskID, *decoded_event, l1_info);
    }

    void destroy_strawalgo(void* object) {
        delete reinterpret_cast<na62::StrawAlgo*>(object);
    }

    // Tracks
    int get_track_n_central(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).ncentrali;
    }
    int get_track_n_lateral(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).nlaterali;
    }
    float get_track_my(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).my;
    }
    float get_track_qy(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).qy;
    }

    float get_track_m1x(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).m1x;
    }
    float get_track_m2x(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).m2x;
    }
    float get_track_q1x(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).q1x;
    }
    float get_track_q2x(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).q2x;
    }
    float get_track_zvertex(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).zvertex;
    }
    float get_track_pz(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).pz;
    }
    float get_track_cda(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).cda;
    }
    float get_track_trailing(void* object, int track_index) {
        return (reinterpret_cast<na62::StrawAlgo*>(object))->getTracks(track_index).trailing;
    }
}






namespace na62 {

uint StrawAlgo::AlgoID_; //0 for CHOD, 1 for RICH, 2 for KTAG, 3 for LAV, 4 for IRCSAC, 5 for Straw, 6 for MUV3, 7 for NewCHOD
uint StrawAlgo::AlgoLogic_[16];
uint StrawAlgo::AlgoRefTimeSourceID_[16];
float StrawAlgo::AlgoOnlineTimeWindow_[16];
float StrawAlgo::AlgoOnlinePNNMomentumCut_[16];

STRAWParsConfFile* StrawAlgo::InfoSTRAW_ = STRAWParsConfFile::GetInstance();
int* StrawAlgo::StrawGeo_ = nullptr;
float* StrawAlgo::ROMezzaninesT0_ = nullptr;
float StrawAlgo::StationT0_ = 0;
float StrawAlgo::MagicT0_ = 0;

const float StrawAlgo::CutLowLeading_ = -5.0;
const float StrawAlgo::CutHighLeading_ = 200.0;
const float StrawAlgo::CutLowTrailing_ = 25;
const float StrawAlgo::CutHighTrailing_ = 275;
const int StrawAlgo::CutCluster_ = 8;
const float StrawAlgo::M1LeadTrail_ = 1;
const float StrawAlgo::Q1LeadTrail_ = 0;
const float StrawAlgo::M2LeadTrail_ = 0;
const float StrawAlgo::Q2LeadTrail_ = -1150.0;
const float StrawAlgo::Hit3Low_ = 8.0;
const float StrawAlgo::Hit3High_ = 9.6;
const float StrawAlgo::Hit2Low_ = 2.7;
const float StrawAlgo::Hit2High_ = 6.0;

const float StrawAlgo::ChamberZPosition_[4] = {ZCHAMBER0, ZCHAMBER1, ZCHAMBER2, ZCHAMBER3 };

// X coordinate of the planes
const float StrawAlgo::XOffset_[4][4] = { { -1058.2, -1067.0, -1071.4, -1062.6 }, { -1062.6, -1071.4, -1067.0, -1058.2 }, { -1058.2,
		-1067.0, -1071.4, -1062.6 }, { -1080.2, -1089.0, -1084.6, -1075.8 } };
const float StrawAlgo::XOffCh_[4][4] = { { -0.0538315, 0.0857196, -0.1735152, 0.0267571 },
		{ 0.2585159, 0.0453096, 0.1563879, -0.01980598 }, { -0.00973369, 0.0212925, -0.3323366, 0.046397608 }, { 0.03975005, 0.1124206,
				-0.02243413, -0.02177299 } };

const float StrawAlgo::HoleChamberMax_[4][4] = { { 134.2, 134.2, 165.0, 63.8 }, { 143.0, 143.0, 178.2, 63.8 },
		{ 129.8, 129.8, 156.2, 63.8 }, { 103.4, 103.4, 116.6, 63.8 } };

const float StrawAlgo::HoleChamberMin_[4][4] = { { 6.6, 6.6, 37.4, -63.8 }, { 15.4, 15.4, 50.6, -63.8 }, { 2.2, 2.2, 28.6, -63.8 }, { -24.2,
		-24.2, -11.0, -63.8 } };

const Point StrawAlgo::QBeam_(0.0, 114.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0);
const Point StrawAlgo::MBeam_(1.0, 0.0012, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0);
const float StrawAlgo::PKaonZ = 75000;
const float StrawAlgo::PKaonX = 75000 * 0.0012;
const float StrawAlgo::PKaonY = 0;
const float StrawAlgo::EKaon = sqrt(MKAON*MKAON + PKaonZ*PKaonZ + PKaonX*PKaonX + PKaonY*PKaonY);

const float StrawAlgo::Sq2_ = sqrt(2);
const float StrawAlgo::InvSq2_ = 1. / Sq2_;

float SigmaRadius(float radius) {
  float sigRad1 = 0.7-0.363*radius;
  float sigRad2 = 0.476-0.147*radius+0.0092*radius*radius+0.00135*radius*radius*radius;
  float sigRad3 = 0.1218;
  if (radius<=0) return 5./sqrt(12.);
  if (radius<1 && radius>0) return sqrt(sigRad1*sigRad1);
  if (radius>=1 && radius<4) return sqrt(sigRad2*sigRad2);
  if (radius>=4) return sqrt(sigRad3*sigRad3);
  return 0.;
}

float SigmaAverage(float x, float y)
{
	float Newsigma = sqrt(x*x + y*y)/2. ;
	return Newsigma;
}

float SigmaAverAndMScat(float x, float y, float dist_point) //metto 10 GeV
{
	float Newsigma = sqrt((x*x + y*y)/4. + (2*SIGMAMULTSCATTERING*dist_point/10.)*(2*SIGMAMULTSCATTERING*dist_point/10.));
	return Newsigma;
}

float SigmaMScat(float dist_point) //metto 10 GeV
{
	float Newsigma = (2*SIGMAMULTSCATTERING*dist_point/10.)*(2*SIGMAMULTSCATTERING*dist_point/10.);
	return Newsigma;
}

float gain(float PrevErr, float MeasureErr)
{
	float NewErr = PrevErr/(PrevErr + MeasureErr);
	return NewErr;
}

float newEstimate(float PrevEst, float Gain, float ExpMeasure)
{
	float NewEstimation = PrevEst + Gain * (ExpMeasure - PrevEst);
	return NewEstimation;
}

float newError( float OldError, float Gain)
{
	float NewError = (1 - Gain) * OldError;
	return NewError;
}

StrawAlgo::StrawAlgo() {
	// Nothing to do at this time
}

void StrawAlgo::initialize(uint i, l1Straw& l1StrawStruct) {

	AlgoID_ = l1StrawStruct.configParams.l1TrigMaskID;
	AlgoLogic_[i] = l1StrawStruct.configParams.l1TrigLogic;
	AlgoRefTimeSourceID_[i] = l1StrawStruct.configParams.l1TrigRefTimeSourceID; //0 for L0TP, 1 for CHOD, 2 for RICH
	AlgoOnlineTimeWindow_[i] = l1StrawStruct.configParams.l1TrigOnlineTimeWindow;
	AlgoOnlinePNNMomentumCut_[i] = l1StrawStruct.PNNMomentumCut;
\
}

  void StrawAlgo::loadConfigurationFile(std::string absolute_chMapFile_path, std::string absolute_coarseT0_path, std::string absolute_magicT0_path) {
	InfoSTRAW_->loadConfigFile(absolute_chMapFile_path);
	InfoSTRAW_->readT0(absolute_coarseT0_path);
	StrawGeo_ = InfoSTRAW_->getGeoMap();
	ROMezzaninesT0_ = InfoSTRAW_->getT0();
	StationT0_ = InfoSTRAW_->getStationT0();
	MagicT0_ = InfoSTRAW_->getMagicT0(absolute_magicT0_path);
}

uint_fast8_t StrawAlgo::processStrawTrigger(uint l0MaskID, DecoderHandler& decoder, L1InfoToStorage* l1Info) {

	const float triggertime = static_cast<float>(decoder.getDecodedEvent()->getFinetime()) * CLOCK_PERIOD / 256 + 0.5;

	using namespace l0;

	int flagL1 = 0;
	int flagL1Pnn = 0;
	int flagL1PnnMUV3Tight = 0;
	int flagL1Exotic = 0;
	int flagL1Mmiss = 0;
	int flagL1MultiTrk = 0;
	int flagL1DVloose = 0;
	int flagL1OneTrack = 0;
	int flagL1ExoticTight = 0;
	int flagL1Limit[1000] = { 0 };
	int flagL1MUV3[1000] = { 0 };
	int flagL1MUV3Tight[1000] = { 0 };
	int flagL1Three[1000] = { 0 };

	uint nEdgesTotal = 0;
	uint nHits = 0;
	bool tlFlags = 0;

	int nTrackIntermedie;
	int nTrackFinal;
	int nTrackFinalExo;

	Track trackIntermedieTemp;

	int tempCondivise;
	int ChambCondivise[4] = { 0 };
	int nChambCondivise = 0;

	int nStrawPointsTemp[4] = { 0 };
	int nStrawPointsFinal[4] = { 0 };
	int nStrawClusters[4][4] = { 0 };
	int nStrawClustersSel[4][4] = { 0 };
	int nStrawPreclusters[4][4][2] = { 0 };

	int *addhit1 = new int[50];
	int *addhit2 = new int[50];

	Point qTrack;
	Point mTrack;
	Point vertex;
	long long hought[RANGEM][RANGEQ] = { 0 };

	l1Info->setL1StrawPNNMomentumCut(AlgoOnlinePNNMomentumCut_[l0MaskID]);

    DecoderRange<SrbFragmentDecoder> packet = decoder.getSTRAWDecoderRange();
    if (packet.begin() == packet.end()) {
        LOG_ERROR("Straw: Empty decoder range!");
        l1Info->setL1StrawBadData();
        return 0;
    }

	for (SrbFragmentDecoder* strawPacket_ : decoder.getSTRAWDecoderRange()) {
		if (not strawPacket_->isReady() or strawPacket_->isBadFragment()) {
			LOG_ERROR("STRAW: This looks like a Bad Packet!!!! ");
			l1Info->setL1StrawBadData();
			return 0;
		}

		/**
		 * Get Arrays with hit Info
		 */
		const uint_fast8_t* strawAddr = strawPacket_->getStrawIDs();
		const float* edgeTime = strawPacket_->getTimes();
		const bool* edgeIsLeading = strawPacket_->getIsLeadings();
		const uint_fast8_t* srbAddr = strawPacket_->getSrbIDs();

		uint numberOfEdgesOfCurrentBoard = strawPacket_->getNumberOfEdgesStored();

		for (uint iEdge = 0; iEdge != numberOfEdgesOfCurrentBoard; iEdge++) {

			tlFlags = 0;

			const int roChID = 256 * srbAddr[iEdge] + strawAddr[iEdge];

			if(roChID < 0)
			{
				LOG_ERROR("STRAW: roChID negative!!!! roChID = "<<roChID<<", iEdge = "<<iEdge);
				continue;
			} 

			int chamberID = StrawGeo_[roChID] / 1952;
			if (chamberID >= 4) {
				return StrawAlgo::abortProcessing(l1Info);
			}
			int viewID = (StrawGeo_[roChID] % 1952) / 488;
			if (viewID >= 4) {
				return StrawAlgo::abortProcessing(l1Info);
			}

			int halfViewID = (StrawGeo_[roChID] % 488) / 244;
			if (halfViewID >= 2) {
				return StrawAlgo::abortProcessing(l1Info);
			}

			int planeID = (StrawGeo_[roChID] % 244) / 122;
			int strawID = StrawGeo_[roChID] % 122;
			float leading = -100000.;
			float trailing = -100000.;
			float wireDistance = -100.0;

			int coverAddr = ((strawAddr[iEdge] & 0xf0) >> 4);

			float timelt_temp = (float) edgeTime[iEdge] - (float) StationT0_ - (float) ROMezzaninesT0_[srbAddr[iEdge] * 16 + coverAddr]
						+ (float) MagicT0_ - triggertime;

			if (edgeIsLeading[iEdge]) {
				leading = timelt_temp;
			}
			else {
				trailing = timelt_temp;
			}

			if(chamberID < 0 or viewID < 0 or halfViewID < 0 or planeID < 0 or strawID < 0)			
			{
				//LOG_ERROR("STRAW: Hit identification negative!!!! chamberID = "<<chamberID<<", viewID = "<<viewID<<", halfViewID = "<<halfViewID<<", planeID = "<<planeID<<", strawID = "<<strawID);
				continue;
			}

			float position = posTubNew(chamberID, viewID, halfViewID * 2 + planeID, strawID);

			//////////////PRECLUSTERING, first leading and last trailing//////////////////////////////

			for (int j = 0; j != nStrawPreclusters[chamberID][viewID][halfViewID]; ++j) {
				if ((strawPrecluster_[chamberID][viewID][halfViewID][j].plane == planeID)
						&& (strawPrecluster_[chamberID][viewID][halfViewID][j].tube == strawID)) {
					tlFlags = 1;
					if ((edgeIsLeading[iEdge])
							&& (leading < strawPrecluster_[chamberID][viewID][halfViewID][j].leading
									|| strawPrecluster_[chamberID][viewID][halfViewID][j].leading <= -10)
							&& (leading > -100 && leading < 300)) {
						if (leading < 1)
							wireDistance = DigiManager::rTDependenceData(1.0);
						else
							wireDistance = DigiManager::rTDependenceData(leading / 1000);
						
						float pos_error = SigmaRadius(wireDistance);

						strawPrecluster_[chamberID][viewID][halfViewID][j].leading = leading;
						strawPrecluster_[chamberID][viewID][halfViewID][j].wiredistance = wireDistance;
						strawPrecluster_[chamberID][viewID][halfViewID][j].error = pos_error;
					} else if ((!edgeIsLeading[iEdge])
							&& (strawPrecluster_[chamberID][viewID][halfViewID][j].trailing < -100
									|| strawPrecluster_[chamberID][viewID][halfViewID][j].trailing < trailing)
							&& (trailing > -100 && trailing < 300)) {
						strawPrecluster_[chamberID][viewID][halfViewID][j].trailing = trailing;
					}
				}
			}
			if (!tlFlags) {
				if (leading > -100)
					if (leading < 1)
						wireDistance = 0.0;
					else
						wireDistance = DigiManager::rTDependenceData(leading / 1000);
				else
					wireDistance = -100.0;

				float pos_error = SigmaRadius(wireDistance);

				strawPrecluster_[chamberID][viewID][halfViewID][nStrawPreclusters[chamberID][viewID][halfViewID]].setStraw(chamberID,
						viewID, halfViewID, planeID, strawID, leading, trailing, 0, srbAddr[iEdge], position, wireDistance, pos_error);
				nStrawPreclusters[chamberID][viewID][halfViewID]++;
				if (nStrawPreclusters[chamberID][viewID][halfViewID] >= MAX_N_PRECLUSTER) {
					return StrawAlgo::abortProcessing(l1Info);
				}
			}

			nHits++;
			if (nHits >= MAX_N_HITS) {
				return StrawAlgo::abortProcessing(l1Info);
			}
		}
		nEdgesTotal += numberOfEdgesOfCurrentBoard;
	}

	if (!nEdgesTotal)
		l1Info->setL1StrawEmptyPacket();


	///////////////////////////////////////Start Clustering inside the view///////////////////////////////////////////////////////

	float positionH = 0.0;
	float positionJ = 0.0;
	float meanDistance = 0.0;
	float deltaDistance = 0.0;
	float deltaDistanceTriplets = 0.0;
	float tempDistance = 0.0;
	float trailingCluster = 0.0;

	for (int i = 0; i < 4; i++) {
		for (int g = 0; g < 4; g++) {
			//triplette 1

			for (int j = 0; j < nStrawPreclusters[i][g][0]; j++) { //hit loop

				// cluster con 3 hit (2 della stessa mezza vista)
				if (((strawPrecluster_[i][g][0][j].leading < (M1LeadTrail_ * strawPrecluster_[i][g][0][j].trailing + Q1LeadTrail_)
						&& strawPrecluster_[i][g][0][j].leading > (M2LeadTrail_ * strawPrecluster_[i][g][0][j].trailing + Q2LeadTrail_)
						&& (strawPrecluster_[i][g][0][j].trailing > CutLowTrailing_
								&& strawPrecluster_[i][g][0][j].trailing < CutHighTrailing_))
						|| strawPrecluster_[i][g][0][j].trailing < -99999)
						&& (strawPrecluster_[i][g][0][j].leading > CutLowLeading_ && strawPrecluster_[i][g][0][j].leading < CutHighLeading_)) {
					for (int h = j + 1; h < nStrawPreclusters[i][g][0]; h++) {
						//i take an hit from the plane 2 o 3

						if (((strawPrecluster_[i][g][0][h].leading < (M1LeadTrail_ * strawPrecluster_[i][g][0][h].trailing + Q1LeadTrail_)
								&& strawPrecluster_[i][g][0][h].leading
										> (M2LeadTrail_ * strawPrecluster_[i][g][0][h].trailing + Q2LeadTrail_)
								&& (strawPrecluster_[i][g][0][h].trailing > CutLowTrailing_
										&& strawPrecluster_[i][g][0][h].trailing < CutHighTrailing_))
								|| strawPrecluster_[i][g][0][h].trailing < -99999)
								&& (strawPrecluster_[i][g][0][h].leading > CutLowLeading_
										&& strawPrecluster_[i][g][0][h].leading < CutHighLeading_)) {
							tempDistance = strawPrecluster_[i][g][0][j].position - strawPrecluster_[i][g][0][h].position;
							if (tempDistance < 9 && tempDistance > -9 && (!strawPrecluster_[i][g][0][h].used or !strawPrecluster_[i][g][0][j].used)) {
								if (tempDistance > 0) {
									positionH = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
									positionJ = strawPrecluster_[i][g][0][j].position - strawPrecluster_[i][g][0][j].wiredistance;
								} else {
									positionH = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
									positionJ = strawPrecluster_[i][g][0][j].position + strawPrecluster_[i][g][0][j].wiredistance;
								}

								meanDistance = (positionH + positionJ) / 2;

								float pos_error = SigmaAverAndMScat(strawPrecluster_[i][g][0][h].error,strawPrecluster_[i][g][0][j].error, 11.0);

								deltaDistanceTriplets = strawPrecluster_[i][g][0][h].wiredistance
										+ strawPrecluster_[i][g][0][j].wiredistance;

								if (strawPrecluster_[i][g][0][h].trailing > -200 && strawPrecluster_[i][g][0][j].trailing > -200)
									trailingCluster = (strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][0][j].trailing) / 2;
								else {
									if (strawPrecluster_[i][g][0][h].trailing <= -200)
										trailingCluster = strawPrecluster_[i][g][0][j].trailing;
									else
										trailingCluster = strawPrecluster_[i][g][0][h].trailing;
								}

								if ((((strawPrecluster_[i][g][0][j].leading > -5 and strawPrecluster_[i][g][0][j].leading < 0 and strawPrecluster_[i][g][0][h].leading > 0) or 
										(strawPrecluster_[i][g][0][h].leading > -5 and strawPrecluster_[i][g][0][h].leading < 0 and strawPrecluster_[i][g][0][j].leading > 0)) and 
										(trailingCluster > 100 and trailingCluster < 225) and (strawPrecluster_[i][g][0][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and
										(strawPrecluster_[i][g][0][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205)) or
										((deltaDistanceTriplets > Hit3Low_ and deltaDistanceTriplets < Hit3High_) and (trailingCluster > 60 and trailingCluster < 250))) {

									strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber, strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistanceTriplets, pos_error, 0);

									nStrawClusters[i][g]++;
									if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawPrecluster_[i][g][0][h].used = 1;
									strawPrecluster_[i][g][0][j].used = 1;

									for (int l = 0; l < nStrawPreclusters[i][g][1]; l++) {
										//i look for the third hit in the others 2 planes

										tempDistance = fabs(meanDistance - strawPrecluster_[i][g][1][l].position);
										if (tempDistance < 5 && !strawPrecluster_[i][g][1][l].used) {
											strawPrecluster_[i][g][1][l].used = 1;
										}
									}
								}
							}
						}
					}
				}
			}
			//triplet 2, change half view

			for (int j = 0; j < nStrawPreclusters[i][g][1]; j++) {
				//hit loop

				// cluster with 3 hit (2 in the same half view)
				if (((strawPrecluster_[i][g][1][j].leading < (M1LeadTrail_ * strawPrecluster_[i][g][1][j].trailing + Q1LeadTrail_)
						&& strawPrecluster_[i][g][1][j].leading > (M2LeadTrail_ * strawPrecluster_[i][g][1][j].trailing + Q2LeadTrail_)
						&& (strawPrecluster_[i][g][1][j].trailing > CutLowTrailing_
								&& strawPrecluster_[i][g][1][j].trailing < CutHighTrailing_))
						|| strawPrecluster_[i][g][1][j].trailing < -99999)
						&& (strawPrecluster_[i][g][1][j].leading > CutLowLeading_ && strawPrecluster_[i][g][1][j].leading < CutHighLeading_)) {
					// End of conditional logic

					for (int h = j + 1; h < nStrawPreclusters[i][g][1]; h++) {
						//i take an hit from the plane 2 o 3

						if (((strawPrecluster_[i][g][1][h].leading < (M1LeadTrail_ * strawPrecluster_[i][g][1][h].trailing + Q1LeadTrail_)
								&& strawPrecluster_[i][g][1][h].leading
										> (M2LeadTrail_ * strawPrecluster_[i][g][1][h].trailing + Q2LeadTrail_)
								&& (strawPrecluster_[i][g][1][h].trailing > CutLowTrailing_
										&& strawPrecluster_[i][g][1][h].trailing < CutHighTrailing_))
								|| strawPrecluster_[i][g][1][h].trailing < -99999)
								&& (strawPrecluster_[i][g][1][h].leading > CutLowLeading_
										&& strawPrecluster_[i][g][1][h].leading < CutHighLeading_)) {
							// End of conditional logic

							tempDistance = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][h].position;

							if (tempDistance < 9 && tempDistance > -9 && (!strawPrecluster_[i][g][1][h].used  or !strawPrecluster_[i][g][1][j].used)) {
								if (tempDistance > 0) {
									positionH = strawPrecluster_[i][g][1][h].position + strawPrecluster_[i][g][1][h].wiredistance;
									positionJ = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
								} else {
									positionH = strawPrecluster_[i][g][1][h].position - strawPrecluster_[i][g][1][h].wiredistance;
									positionJ = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
								}

								meanDistance = (positionH + positionJ) / 2;

								float pos_error = SigmaAverAndMScat(strawPrecluster_[i][g][1][h].error,strawPrecluster_[i][g][1][j].error,11.0);

								deltaDistanceTriplets = strawPrecluster_[i][g][1][h].wiredistance
										+ strawPrecluster_[i][g][1][j].wiredistance;

								if (strawPrecluster_[i][g][1][h].trailing > -200 && strawPrecluster_[i][g][1][j].trailing > -200) {
									trailingCluster = (strawPrecluster_[i][g][1][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
								} else {
									if (strawPrecluster_[i][g][1][h].trailing <= -200) {
										trailingCluster = strawPrecluster_[i][g][1][j].trailing;
									} else {
										trailingCluster = strawPrecluster_[i][g][1][h].trailing;
									}
								}

								if ((((strawPrecluster_[i][g][1][j].leading > -5 and strawPrecluster_[i][g][1][j].leading < 0 and strawPrecluster_[i][g][1][h].leading > 0) or
									 (strawPrecluster_[i][g][1][h].leading > -5 and strawPrecluster_[i][g][1][h].leading < 0 and strawPrecluster_[i][g][1][j].leading > 0)) and 
									 (trailingCluster > 100 and trailingCluster < 225) and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][1][h].trailing>120) and 
									 (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][1][h].trailing<205)) or ((deltaDistanceTriplets > Hit3Low_ and deltaDistanceTriplets < Hit3High_) and (trailingCluster > 60 and trailingCluster < 250))) {
									strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][1][h].chamber, strawPrecluster_[i][g][1][h].view, meanDistance, trailingCluster, deltaDistanceTriplets, pos_error, 0);

									nStrawClusters[i][g]++;
									if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawPrecluster_[i][g][1][h].used = 1;
									strawPrecluster_[i][g][1][j].used = 1;

									for (int l = 0; l < nStrawPreclusters[i][g][0]; l++) {
										//i look for the third hit in the others 2 planes

										tempDistance = fabs(meanDistance - strawPrecluster_[i][g][0][l].position);
										if (tempDistance < 5 && !strawPrecluster_[i][g][0][l].used) {
											strawPrecluster_[i][g][0][l].used = 1;
										}
									}
								}
							}
						}
					}

					//cluster with 2 hit
					for (int h = 0; h < nStrawPreclusters[i][g][0]; h++) {

						if (((strawPrecluster_[i][g][0][h].leading < (M1LeadTrail_ * strawPrecluster_[i][g][0][h].trailing + Q1LeadTrail_)
								&& strawPrecluster_[i][g][0][h].leading
										> (M2LeadTrail_ * strawPrecluster_[i][g][0][h].trailing + Q2LeadTrail_)
								&& (strawPrecluster_[i][g][0][h].trailing > CutLowTrailing_
										&& strawPrecluster_[i][g][0][h].trailing < CutHighTrailing_))
								|| strawPrecluster_[i][g][0][h].trailing < -99999)
								&& (strawPrecluster_[i][g][0][h].leading > CutLowLeading_
										&& strawPrecluster_[i][g][0][h].leading < CutHighLeading_)) {
							// End of conditional logic

							tempDistance = fabs(strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][0][h].position);
							if (tempDistance < 9 && (!strawPrecluster_[i][g][0][h].used or !strawPrecluster_[i][g][1][j].used)) {
								switch (strawPrecluster_[i][g][1][j].plane) {
								case (0):
									if (!strawPrecluster_[i][g][0][h].plane) {
										if (!g || g == 2) {
											positionH = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
										} else {
											positionH = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
										}

										meanDistance = (positionH + positionJ) / 2;

										float pos_error = SigmaAverAndMScat(strawPrecluster_[i][g][0][h].error,strawPrecluster_[i][g][1][j].error, 26.0);

										deltaDistance = strawPrecluster_[i][g][0][h].wiredistance
												+ strawPrecluster_[i][g][1][j].wiredistance;

										if (strawPrecluster_[i][g][0][h].trailing > -200 && strawPrecluster_[i][g][1][j].trailing > -200) {
											trailingCluster =
													(strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
										} else {
											if (strawPrecluster_[i][g][0][h].trailing <= -200) {
												trailingCluster = strawPrecluster_[i][g][1][j].trailing;
											} else {
												trailingCluster = strawPrecluster_[i][g][0][h].trailing;
											}
										}

										if ((((strawPrecluster_[i][g][1][j].leading > -5 and strawPrecluster_[i][g][1][j].leading < 0 and strawPrecluster_[i][g][0][h].leading > 0) or (strawPrecluster_[i][g][0][h].leading > -5 and strawPrecluster_[i][g][0][h].leading < 0 and strawPrecluster_[i][g][1][j].leading > 0)) and (trailingCluster > 100 and trailingCluster < 225) and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205)) or ((deltaDistance > Hit2Low_ and deltaDistance < Hit2High_) and (trailingCluster > 60 and trailingCluster < 250))) {
											strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
													strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
											nStrawClusters[i][g]++;
											if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
												return StrawAlgo::abortProcessing(l1Info);
											}
											strawPrecluster_[i][g][0][h].used = 1;
											strawPrecluster_[i][g][1][j].used = 1;
										}
									} else {
										if (!g || g == 2) {
											positionH = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
										} else {
											positionH = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
										}

										meanDistance = (positionH + positionJ) / 2;

										float pos_error = SigmaAverAndMScat(strawPrecluster_[i][g][0][h].error,strawPrecluster_[i][g][1][j].error, 15.0);

										deltaDistance = strawPrecluster_[i][g][0][h].wiredistance
												+ strawPrecluster_[i][g][1][j].wiredistance;

										if (strawPrecluster_[i][g][0][h].trailing > -200 && strawPrecluster_[i][g][1][j].trailing > -200)
											trailingCluster =
													(strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
										else {
											if (strawPrecluster_[i][g][0][h].trailing <= -200) {
												trailingCluster = strawPrecluster_[i][g][1][j].trailing;
											} else {
												trailingCluster = strawPrecluster_[i][g][0][h].trailing;
											}
										}

										if ((((strawPrecluster_[i][g][1][j].leading > -5 and strawPrecluster_[i][g][1][j].leading < 0 and strawPrecluster_[i][g][0][h].leading > 0) or (strawPrecluster_[i][g][0][h].leading > -5 and strawPrecluster_[i][g][0][h].leading < 0 and strawPrecluster_[i][g][1][j].leading > 0))  and (trailingCluster > 100 and trailingCluster < 225) and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205)) or ((deltaDistance > Hit2Low_ and deltaDistance < Hit2High_) and (trailingCluster > 60 and trailingCluster < 250))) {
											strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
													strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
											nStrawClusters[i][g]++;
											if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
												return StrawAlgo::abortProcessing(l1Info);
											}
											strawPrecluster_[i][g][0][h].used = 1;
											strawPrecluster_[i][g][1][j].used = 1;
										}
									}

									break;
								case (1):
									if (!strawPrecluster_[i][g][0][h].plane) {
										if (!g || g == 2) {
											positionH = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
										} else {
											positionH = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
										}

										meanDistance = (positionH + positionJ) / 2;

										float pos_error = SigmaAverAndMScat(strawPrecluster_[i][g][0][h].error,strawPrecluster_[i][g][1][j].error, 37.0);

										deltaDistance = strawPrecluster_[i][g][0][h].wiredistance
												+ strawPrecluster_[i][g][1][j].wiredistance;

										if (strawPrecluster_[i][g][0][h].trailing > -200 && strawPrecluster_[i][g][1][j].trailing > -200)
											trailingCluster =
													(strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
										else {
											if (strawPrecluster_[i][g][0][h].trailing <= -200)
												trailingCluster = strawPrecluster_[i][g][1][j].trailing;
											else
												trailingCluster = strawPrecluster_[i][g][0][h].trailing;
										}

										if ((((strawPrecluster_[i][g][1][j].leading > -5 and strawPrecluster_[i][g][1][j].leading < 0 and strawPrecluster_[i][g][0][h].leading > 0) or (strawPrecluster_[i][g][0][h].leading > -5 and strawPrecluster_[i][g][0][h].leading < 0 and strawPrecluster_[i][g][1][j].leading > 0)) and (trailingCluster > 100 and trailingCluster < 225) and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205)) or ((deltaDistance > Hit2Low_ and deltaDistance < Hit2High_) and (trailingCluster > 60 and trailingCluster < 250))) {
											strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
													strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
											nStrawClusters[i][g]++;
											if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
												return StrawAlgo::abortProcessing(l1Info);
											}
											strawPrecluster_[i][g][0][h].used = 1;
											strawPrecluster_[i][g][1][j].used = 1;
										}
									} else {
										if (!g || g == 2) {
											positionH = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
										} else {
											positionH = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
											positionJ = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
										}

										meanDistance = (positionH + positionJ) / 2;

										float pos_error = SigmaAverAndMScat(strawPrecluster_[i][g][0][h].error,strawPrecluster_[i][g][1][j].error, 26.0);

										deltaDistance = strawPrecluster_[i][g][0][h].wiredistance
												+ strawPrecluster_[i][g][1][j].wiredistance;

										if (strawPrecluster_[i][g][0][h].trailing > -200 && strawPrecluster_[i][g][1][j].trailing > -200)
											trailingCluster =
													(strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
										else {
											if (strawPrecluster_[i][g][0][h].trailing <= -200) {
												trailingCluster = strawPrecluster_[i][g][1][j].trailing;
											} else {
												trailingCluster = strawPrecluster_[i][g][0][h].trailing;
											}
										}

										if ((((strawPrecluster_[i][g][1][j].leading > -5 and strawPrecluster_[i][g][1][j].leading < 0 and strawPrecluster_[i][g][0][h].leading > 0) or (strawPrecluster_[i][g][0][h].leading > -5 and strawPrecluster_[i][g][0][h].leading < 0 and strawPrecluster_[i][g][1][j].leading > 0)) and (trailingCluster > 100 and trailingCluster < 225) and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205)) or ((deltaDistance > Hit2Low_ and deltaDistance < Hit2High_) and (trailingCluster > 60 and trailingCluster < 250))) {
											strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
													strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
											nStrawClusters[i][g]++;
											if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
												return StrawAlgo::abortProcessing(l1Info);
											}
											strawPrecluster_[i][g][0][h].used = 1;
											strawPrecluster_[i][g][1][j].used = 1;
										}
									}
									break;
								}
							}
						}
					}
				}
			}

			//recover cluster with 2 hit (1 leading absent)
			for (int h = 0; h < nStrawPreclusters[i][g][0]; h++) {
				for (int j = 0; j < nStrawPreclusters[i][g][1] && !strawPrecluster_[i][g][0][h].used; j++) {	
					
					tempDistance = fabs(strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][0][h].position);

					if(!strawPrecluster_[i][g][1][j].used
							&& ((strawPrecluster_[i][g][0][h].leading < -99999
									&& strawPrecluster_[i][g][1][j].leading < (M1LeadTrail_ * strawPrecluster_[i][g][1][j].trailing + Q1LeadTrail_)
									&& strawPrecluster_[i][g][1][j].leading > (M2LeadTrail_ * strawPrecluster_[i][g][1][j].trailing + Q2LeadTrail_)
									&& strawPrecluster_[i][g][1][j].leading > 0 && strawPrecluster_[i][g][1][j].leading < CutHighLeading_) 
								or ( strawPrecluster_[i][g][1][j].leading < -99999
									&& strawPrecluster_[i][g][0][h].leading < (M1LeadTrail_ * strawPrecluster_[i][g][0][h].trailing + Q1LeadTrail_)
									&& strawPrecluster_[i][g][0][h].leading > (M2LeadTrail_ * strawPrecluster_[i][g][0][h].trailing + Q2LeadTrail_)
									&& strawPrecluster_[i][g][0][h].leading > 0 && strawPrecluster_[i][g][0][h].leading < CutHighLeading_))
							&& strawPrecluster_[i][g][0][h].trailing > CutLowTrailing_ && strawPrecluster_[i][g][0][h].trailing < CutHighTrailing_
							&& strawPrecluster_[i][g][1][j].trailing > CutLowTrailing_ && strawPrecluster_[i][g][1][j].trailing < CutHighTrailing_
							&& tempDistance < 9){

						switch (strawPrecluster_[i][g][1][j].plane) {
						case (0):
							if (!strawPrecluster_[i][g][0][h].plane) {

								float pos_error = -100;
								if(strawPrecluster_[i][g][1][j].leading < -99999) pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][0][h].error,0,26.0); //devo cmq mettere il multiplo scattering
								else pos_error =  SigmaAverAndMScat(2*strawPrecluster_[i][g][1][j].error,0,26.0); 

								if (!g || g == 2) {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
								} else {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
								}

								trailingCluster = (strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;

								deltaDistance = -1000;

								if(trailingCluster > 100 and trailingCluster < 225 and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205))
								{
									strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
											strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
									nStrawClusters[i][g]++;
									if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawPrecluster_[i][g][0][h].used = 1;
									strawPrecluster_[i][g][1][j].used = 1;
								}

							} else {

								float pos_error = -100;
								if(strawPrecluster_[i][g][1][j].leading < -99999) pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][0][h].error,0,15.0);
								else  pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][1][j].error,0,15.0);

								if (!g || g == 2) {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
								} else {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
								}

								trailingCluster = (strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
								deltaDistance = -1000;

								if(trailingCluster > 100 and trailingCluster < 225 and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205))
								{
									strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
											strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
									nStrawClusters[i][g]++;
									if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawPrecluster_[i][g][0][h].used = 1;
									strawPrecluster_[i][g][1][j].used = 1;
								}
				
							}

							break;
						case (1):
							if (!strawPrecluster_[i][g][0][h].plane) {

								float pos_error = -100;
								if(strawPrecluster_[i][g][1][j].leading < -99999) pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][0][h].error,0,37.0);
								else  pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][1][j].error,0,37.0);

								if (!g || g == 2) {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
								} else {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
								}

								trailingCluster = (strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
								deltaDistance = -1000;

								if(trailingCluster > 100 and trailingCluster < 225 and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205))
								{
									strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
											strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
									nStrawClusters[i][g]++;
									if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawPrecluster_[i][g][0][h].used = 1;
									strawPrecluster_[i][g][1][j].used = 1;
								}

							} else {

								float pos_error = -100;
								if(strawPrecluster_[i][g][1][j].leading < -99999) pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][0][h].error,0,26.0);
								else  pos_error = SigmaAverAndMScat(2*strawPrecluster_[i][g][1][j].error,0,26.0);

								if (!g || g == 2) {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position + strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position - strawPrecluster_[i][g][1][j].wiredistance;
								} else {
									if(strawPrecluster_[i][g][1][j].leading < -99999) meanDistance = strawPrecluster_[i][g][0][h].position - strawPrecluster_[i][g][0][h].wiredistance;
									else meanDistance = strawPrecluster_[i][g][1][j].position + strawPrecluster_[i][g][1][j].wiredistance;
								}

								trailingCluster = (strawPrecluster_[i][g][0][h].trailing + strawPrecluster_[i][g][1][j].trailing) / 2;
								deltaDistance = -1000;

								if(trailingCluster > 100 and trailingCluster < 225 and (strawPrecluster_[i][g][1][j].trailing>120 or strawPrecluster_[i][g][0][h].trailing>120) and (strawPrecluster_[i][g][1][j].trailing<205 or strawPrecluster_[i][g][0][h].trailing<205))
								{
									strawCluster_[i][g][nStrawClusters[i][g]].setCluster(strawPrecluster_[i][g][0][h].chamber,
											strawPrecluster_[i][g][0][h].view, meanDistance, trailingCluster, deltaDistance, pos_error, 0);
									nStrawClusters[i][g]++;
									if (nStrawClusters[i][g] >= MAX_N_CLUSTER) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawPrecluster_[i][g][0][h].used = 1;
									strawPrecluster_[i][g][1][j].used = 1;
								}
							}
							break;
						}

					}
				}
			}
		}
	}

	/////// average on similar clusters

	for (int i = 0; i < 4; i++) {
		for (int c = 0; c < 4; c++) {
			for (int a = 0; a < nStrawClusters[i][c]; a++) { 
				if(strawCluster_[i][c][a].used == 0)
				{
					int nAddCluster = 1;
					float trailing_temp = strawCluster_[i][c][a].trailing;
					float coordinate_temp = strawCluster_[i][c][a].coordinate;
					float pos_error_temp = strawCluster_[i][c][a].error;

					for (int b = a+1; b < nStrawClusters[i][c]; b++) { 
						float DeltaCoordinate = fabs(coordinate_temp - strawCluster_[i][c][b].coordinate);
						if(DeltaCoordinate<4.5 and strawCluster_[i][c][b].used == 0)
						{
							coordinate_temp += strawCluster_[i][c][b].coordinate;
							trailing_temp += strawCluster_[i][c][b].trailing;
							pos_error_temp += strawCluster_[i][c][b].error;
							strawCluster_[i][c][b].used = 1;
							nAddCluster++;
						}
					}

					float trailing_mean = trailing_temp / nAddCluster; 
					float coordinate_mean = coordinate_temp / nAddCluster;
					float pos_error_mean = pos_error_temp / nAddCluster;

					strawClusterSel_[i][c][nStrawClustersSel[i][c]].setCluster(i,c, coordinate_mean, trailing_mean, strawCluster_[i][c][a].deltadistance, pos_error_mean, 0);
					nStrawClustersSel[i][c]++;

				}
			}
		}
	}

	/////////////////////////////////////// Start Clustering inside the chamber ///////////////////////////////////////////////////////
	/////////////////////////////////////// 0=v, 1=u, 2=x, 3=y

	float coordinateTemp = 0.0;
	float viewDistance = 0.0;
	float viewDistance3 = 0.0;
	float viewDistance4 = 0.0;
	float xTemp = 0.0;
	float yTemp = 0.0;
	float trailingpoint = 0.0;

	//tracks reco
	float qy = 0.0;

	for (int i = 0; i < 4; i++) { //0 v, 1 u, 2 x, 3 y
		////////////loop over the points of 4 viste//////////////////
		for (int a = 0; a < nStrawClustersSel[i][3]; a++) { //clusters [a] is inside y
			for (int b = 0; b < nStrawClustersSel[i][2]; b++) { //we loop on x view
				for (int c = 0; c < nStrawClustersSel[i][1]; c++) { //we loop on u  views
					coordinateTemp = (strawClusterSel_[i][3][a].coordinate + strawClusterSel_[i][2][b].coordinate) / Sq2_;
					viewDistance3 = fabs(strawClusterSel_[i][1][c].coordinate - coordinateTemp);					
					if (viewDistance3 < CutCluster_) {
						for (int d = 0; d < nStrawClustersSel[i][0]; d++) { //v views
							coordinateTemp = (strawClusterSel_[i][2][b].coordinate - strawClusterSel_[i][3][a].coordinate) / Sq2_;
							viewDistance4 = fabs(strawClusterSel_[i][0][d].coordinate - coordinateTemp);
							if (viewDistance4 < CutCluster_) {
								xTemp = strawClusterSel_[i][2][b].coordinate;
								yTemp = strawClusterSel_[i][3][a].coordinate;

								// Decide which of the two we want to actually save
								viewDistance = viewDistance3 < viewDistance4 ? viewDistance3 : viewDistance4;

								trailingpoint = (strawClusterSel_[i][3][a].trailing + strawClusterSel_[i][2][b].trailing + strawClusterSel_[i][1][c].trailing
												+ strawClusterSel_[i][0][d].trailing) / 4;

								float bestViewDistance = 1000;

								for (int e = 0; e < nStrawPointsTemp[i]; e++) { //loop on previous point 4 views

									int nshared = 0;
									if(strawPointTemp_[i][e].views[0] == d) nshared++;
									if(strawPointTemp_[i][e].views[1] == c) nshared++;
									if(strawPointTemp_[i][e].views[2] == b) nshared++;
									if(strawPointTemp_[i][e].views[3] == a) nshared++;

									if(nshared>2 and bestViewDistance > strawPointTemp_[i][e].viewDistance) bestViewDistance = strawPointTemp_[i][e].viewDistance;
								}
													
								if(viewDistance < bestViewDistance){

									float point_errorx = sqrt((strawClusterSel_[i][2][b].error*strawClusterSel_[i][2][b].error) + 2*SigmaMScat(DZHVIEWS) + SigmaMScat(DZTVIEWS)); //i add the mult-scattering for 4 straw
									float point_errory = sqrt((strawClusterSel_[i][3][a].error*strawClusterSel_[i][3][a].error) + 2*SigmaMScat(DZHVIEWS) + SigmaMScat(DZTVIEWS)); //i add the mult-scattering for 4 straw

									strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], xTemp, yTemp, trailingpoint, viewDistance, point_errorx, point_errory, 4, 0);

									strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = d;
									strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = c;
									strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = b;
									strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = a;

									strawClusterSel_[i][0][d].used = 1;
									strawClusterSel_[i][1][c].used = 1;
									strawClusterSel_[i][2][b].used = 1;
									strawClusterSel_[i][3][a].used = 1;

									nStrawPointsTemp[i]++;

									if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
										return StrawAlgo::abortProcessing(l1Info);
									}
								}
							}
						} // End of d/v loop
					} // End of cluster cut conditional
				} // End of c/u loop
			} // End of b/x loop
		} // End of a loop

		//////////////////////////// loop 3 views point, first yx looking for v o u, then uv looking for x o y

		for (int a = 0; a < nStrawClustersSel[i][3]; a++) { //clusters [a] is inside y
			for (int b = 0; b < nStrawClustersSel[i][2]; b++) { //we loop on x view
				for (int c = 0; c < nStrawClustersSel[i][1]; c++) { //we loop on u  views
					if (strawClusterSel_[i][1][c].used == 0 || strawClusterSel_[i][3][a].used == 0 || strawClusterSel_[i][2][b].used == 0) 
					{
						coordinateTemp = (strawClusterSel_[i][3][a].coordinate + strawClusterSel_[i][2][b].coordinate) / Sq2_;
						viewDistance = fabs(strawClusterSel_[i][1][c].coordinate - coordinateTemp);

						if (viewDistance < CutCluster_) {
							xTemp = strawClusterSel_[i][2][b].coordinate;
							yTemp = strawClusterSel_[i][3][a].coordinate;
							trailingpoint = (strawClusterSel_[i][3][a].trailing + strawClusterSel_[i][2][b].trailing + strawClusterSel_[i][1][c].trailing)/3;

							float point_errorx = sqrt((strawClusterSel_[i][2][b].error*strawClusterSel_[i][2][b].error) + SigmaMScat(DZHVIEWS) + SigmaMScat(DZHVIEWS+DZTVIEWS)); //i add the mult-scattering for 4 straw
							float point_errory = sqrt((strawClusterSel_[i][3][a].error*strawClusterSel_[i][3][a].error) + SigmaMScat(DZHVIEWS) + SigmaMScat(DZHVIEWS+DZTVIEWS)); //i add the mult-scattering for 4 straw

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], xTemp, yTemp, trailingpoint, viewDistance, point_errorx, point_errory, 3, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = c;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = b;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = a;

							if (trailingpoint>110 && trailingpoint<190 && viewDistance<6)
							{
								strawClusterSel_[i][1][c].used = 2;
								strawClusterSel_[i][2][b].used = 2;
								strawClusterSel_[i][3][a].used = 2;
							}

							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				} // End of c/u loop
				for (int c = 0; c < nStrawClustersSel[i][0]; c++) { //we loop on v views

					if (strawClusterSel_[i][0][c].used == 0 || strawClusterSel_[i][3][a].used == 0 || strawClusterSel_[i][2][b].used == 0) {

						coordinateTemp = (strawClusterSel_[i][2][b].coordinate - strawClusterSel_[i][3][a].coordinate) / Sq2_;
						viewDistance = fabs(strawClusterSel_[i][0][c].coordinate - coordinateTemp);

						if (viewDistance < CutCluster_) {
							xTemp = strawClusterSel_[i][2][b].coordinate;
							yTemp = strawClusterSel_[i][3][a].coordinate;
							trailingpoint = (strawClusterSel_[i][3][a].trailing + strawClusterSel_[i][2][b].trailing + strawClusterSel_[i][0][c].trailing)/ 3;

							float point_errorx = sqrt((strawClusterSel_[i][2][b].error*strawClusterSel_[i][2][b].error) + SigmaMScat(DZHVIEWS) + SigmaMScat(DZTVIEWS)); //i add the mult-scattering for 4 straw
							float point_errory = sqrt((strawClusterSel_[i][3][a].error*strawClusterSel_[i][3][a].error) + SigmaMScat(DZHVIEWS) + SigmaMScat(DZTVIEWS)); //i add the mult-scattering for 4 straw

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], xTemp, yTemp, trailingpoint, viewDistance, point_errorx, point_errory,3, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = c;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = b;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = a;

							if (trailingpoint>110 && trailingpoint<190 && viewDistance<6)
							{
								strawClusterSel_[i][0][c].used = 2;
								strawClusterSel_[i][2][b].used = 2;
								strawClusterSel_[i][3][a].used = 2;
							}
							
							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				} // End of c/v loop
			} // End of b/x loop
		} // End of a loop

		/////////////////////////3 viste seconda clusterizzazione partendo da u e v

		for (int a = 0; a < nStrawClustersSel[i][0]; a++) { //v
			for (int b = 0; b < nStrawClustersSel[i][1]; b++) { //u
				for (int c = 0; c < nStrawClustersSel[i][2]; c++) { //x

					if (strawClusterSel_[i][0][a].used == 0 || strawClusterSel_[i][2][c].used == 0 || strawClusterSel_[i][1][b].used == 0) {
						// End of conditional logic

						coordinateTemp = (strawClusterSel_[i][0][a].coordinate + strawClusterSel_[i][1][b].coordinate) / Sq2_;
						viewDistance = fabs(strawClusterSel_[i][2][c].coordinate - coordinateTemp);

						if (viewDistance < CutCluster_) {
							xTemp = strawClusterSel_[i][2][c].coordinate;
							yTemp = (strawClusterSel_[i][1][b].coordinate - strawClusterSel_[i][0][a].coordinate) / Sq2_;
							trailingpoint = (strawClusterSel_[i][0][a].trailing + strawClusterSel_[i][1][b].trailing + strawClusterSel_[i][2][c].trailing)/ 3;

							float point_errorx = sqrt((strawClusterSel_[i][2][c].error*strawClusterSel_[i][2][c].error) + SigmaMScat(DZHVIEWS) + SigmaMScat(DZTVIEWS)); //i add the mult-scattering for 4 straw
							float point_errory = sqrt((strawClusterSel_[i][1][b].error * strawClusterSel_[i][1][b].error + strawClusterSel_[i][0][a].error * strawClusterSel_[i][0][a].error)/2 + SigmaMScat(DZHVIEWS) + SigmaMScat(DZTVIEWS));

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], xTemp, yTemp, trailingpoint, viewDistance, point_errorx, point_errory,3, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = b;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = c;
						
							if (trailingpoint>110 && trailingpoint<190 && viewDistance<5)
							{
								strawClusterSel_[i][0][a].used = 2;
								strawClusterSel_[i][1][b].used = 2;
								strawClusterSel_[i][2][c].used = 2;
							}
							
							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}

				for (int c = 0; c < nStrawClustersSel[i][3]; c++){ //y
					if (strawClusterSel_[i][1][b].used == 0 || strawClusterSel_[i][0][a].used == 0 || strawClusterSel_[i][3][c].used == 0) {
						// End of condititional logic

						coordinateTemp = (strawClusterSel_[i][1][b].coordinate - strawClusterSel_[i][0][a].coordinate) / Sq2_;
						viewDistance = fabs(strawClusterSel_[i][3][c].coordinate - coordinateTemp);

						if (viewDistance < CutCluster_) {
							xTemp = (strawClusterSel_[i][0][a].coordinate + strawClusterSel_[i][1][b].coordinate) / Sq2_;
							yTemp = strawClusterSel_[i][3][c].coordinate;
							trailingpoint = (strawClusterSel_[i][0][a].trailing + strawClusterSel_[i][1][b].trailing + strawClusterSel_[i][3][c].trailing)/ 3;

							float point_errorx = sqrt((strawClusterSel_[i][1][b].error * strawClusterSel_[i][1][b].error + strawClusterSel_[i][0][a].error * strawClusterSel_[i][0][a].error)/2 + SigmaMScat(DZHVIEWS) + SigmaMScat(DZHVIEWS+DZTVIEWS));
							float point_errory = sqrt((strawClusterSel_[i][3][c].error*strawClusterSel_[i][3][c].error) + SigmaMScat(DZHVIEWS) + SigmaMScat(DZHVIEWS+DZTVIEWS)); //i add the mult-scattering for 4 straw

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], xTemp, yTemp, trailingpoint, viewDistance, point_errorx, point_errory, 3, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = b;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = c;

							if (trailingpoint>110 && trailingpoint<190 && viewDistance<5)
							{
								strawClusterSel_[i][0][a].used = 2;
								strawClusterSel_[i][1][b].used = 2;
								strawClusterSel_[i][3][c].used = 2;
							}
							
							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
		}

		// 2 views points

		for (int a = 0; a < nStrawClustersSel[i][0]; a++) { //v

			for (int b = 0; b < nStrawClustersSel[i][1]; b++) { //u (v,u)

				if (strawClusterSel_[i][1][b].used == 0 or strawClusterSel_[i][0][a].used == 0) {

					float coord_v = strawClusterSel_[i][0][a].coordinate;
					float coord_u = strawClusterSel_[i][1][b].coordinate;
					float coord_x = (strawClusterSel_[i][0][a].coordinate + strawClusterSel_[i][1][b].coordinate) / Sq2_;
					float coord_y = (strawClusterSel_[i][1][b].coordinate - strawClusterSel_[i][0][a].coordinate) / Sq2_;

					if (strawAcceptance(i, coord_v, coord_u, coord_x, coord_y, 0) == 1) {

						int flag_accident = 0;

						for (int c = 0; c < nStrawPointsTemp[i]; c++)
						{
							if((strawPointTemp_[i][c].views[0] == a or strawPointTemp_[i][c].views[1] == b) and strawPointTemp_[i][c].nViews>2 and strawPointTemp_[i][c].viewDistance < 2) flag_accident = 1;
						}

						if(flag_accident == 0)
						{
							trailingpoint = (strawClusterSel_[i][0][a].trailing + strawClusterSel_[i][1][b].trailing) / 2;

							float point_error = sqrt((strawClusterSel_[i][1][b].error * strawClusterSel_[i][1][b].error + strawClusterSel_[i][0][a].error * strawClusterSel_[i][0][a].error)/2 + SigmaMScat(DZHVIEWS));

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], coord_x, coord_y, trailingpoint, 0, point_error, point_error, 2, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = b;

							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
			for (int b = 0; b < nStrawClustersSel[i][2]; b++) { //x (v,x)

				if (strawClusterSel_[i][2][b].used == 0 or strawClusterSel_[i][0][a].used == 0) {

					float coord_v = strawClusterSel_[i][0][a].coordinate;
					float coord_u = (Sq2_ * strawClusterSel_[i][2][b].coordinate) - strawClusterSel_[i][0][a].coordinate;
					float coord_x = strawClusterSel_[i][2][b].coordinate;
					float coord_y = strawClusterSel_[i][2][b].coordinate - (Sq2_ * strawClusterSel_[i][0][a].coordinate);
					
					if (strawAcceptance(i, coord_v, coord_u, coord_x, coord_y, 1) == 1) {

						int flag_accident = 0;

						for (int c = 0; c < nStrawPointsTemp[i]; c++)
						{
							if((strawPointTemp_[i][c].views[0] == a or strawPointTemp_[i][c].views[2] == b) and strawPointTemp_[i][c].nViews>2 and strawPointTemp_[i][c].viewDistance < 2) flag_accident = 1;
						}

						if(flag_accident == 0)
						{
							trailingpoint = (strawClusterSel_[i][0][a].trailing + strawClusterSel_[i][2][b].trailing) / 2;

							float point_errorx = sqrt((strawClusterSel_[i][2][b].error*strawClusterSel_[i][2][b].error) + SigmaMScat(DZTVIEWS)); //i add the mult-scattering for 4 straw
							float point_errory = sqrt(strawClusterSel_[i][2][b].error * strawClusterSel_[i][2][b].error + 2 * strawClusterSel_[i][0][a].error * strawClusterSel_[i][0][a].error  + SigmaMScat(DZTVIEWS));

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], coord_x, coord_y, trailingpoint, 0, point_errorx, point_errory, 2, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = b;
				
							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
			for (int b = 0; b < nStrawClustersSel[i][3]; b++) { //y (v,y)

				if (strawClusterSel_[i][3][b].used == 0 or strawClusterSel_[i][0][a].used == 0) {

					float coord_v = strawClusterSel_[i][0][a].coordinate;
					float coord_u = (Sq2_ * strawClusterSel_[i][3][b].coordinate) + strawClusterSel_[i][0][a].coordinate;
					float coord_x = (Sq2_ * strawClusterSel_[i][0][a].coordinate) + strawClusterSel_[i][3][b].coordinate;
					float coord_y = strawClusterSel_[i][3][b].coordinate;

					if (strawAcceptance(i, coord_v, coord_u, coord_x, coord_y, 2) == 1) {

						int flag_accident = 0;

						for (int c = 0; c < nStrawPointsTemp[i]; c++)
						{
							if((strawPointTemp_[i][c].views[0] == a or strawPointTemp_[i][c].views[3] == b) and strawPointTemp_[i][c].nViews>2 and strawPointTemp_[i][c].viewDistance < 2) flag_accident = 1;
						}

						if(flag_accident == 0)
						{
							trailingpoint = (strawClusterSel_[i][0][a].trailing + strawClusterSel_[i][3][b].trailing) / 2;

							float point_errorx = sqrt(strawClusterSel_[i][3][b].error * strawClusterSel_[i][3][b].error + 2 * strawClusterSel_[i][0][a].error * strawClusterSel_[i][0][a].error  + SigmaMScat(DZHVIEWS+DZTVIEWS));
							float point_errory = sqrt((strawClusterSel_[i][3][b].error*strawClusterSel_[i][3][b].error) + SigmaMScat(DZHVIEWS+DZTVIEWS)); //i add the mult-scattering for 4 straw

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], coord_x, coord_y, trailingpoint, 0, point_errorx, point_errory, 2, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[0] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = b;

							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
		}
		for (int a = 0; a < nStrawClustersSel[i][1]; a++) { //u

			for (int b = 0; b < nStrawClustersSel[i][2]; b++) { //x (u,x)

				if (strawClusterSel_[i][2][b].used == 0 or strawClusterSel_[i][1][a].used == 0) {

					float coord_v = (Sq2_ * strawClusterSel_[i][2][b].coordinate) - strawClusterSel_[i][1][a].coordinate;
					float coord_u = strawClusterSel_[i][1][a].coordinate;
					float coord_x = strawClusterSel_[i][2][b].coordinate;
					float coord_y = (Sq2_ * strawClusterSel_[i][1][a].coordinate) - strawClusterSel_[i][2][b].coordinate;

					if (strawAcceptance(i, coord_v, coord_u, coord_x, coord_y, 3) == 1) {

						int flag_accident = 0;

						for (int c = 0; c < nStrawPointsTemp[i]; c++)
						{
							if((strawPointTemp_[i][c].views[1] == a or strawPointTemp_[i][c].views[2] == b) and strawPointTemp_[i][c].nViews>2 and strawPointTemp_[i][c].viewDistance < 2) flag_accident = 1;
						}

						if(flag_accident == 0)
						{
							trailingpoint = (strawClusterSel_[i][1][a].trailing + strawClusterSel_[i][2][b].trailing) / 2;

							float point_errorx = sqrt((strawClusterSel_[i][2][b].error*strawClusterSel_[i][2][b].error) + SigmaMScat(DZHVIEWS+DZTVIEWS));
							float point_errory = sqrt(strawClusterSel_[i][2][b].error * strawClusterSel_[i][2][b].error + 2 * strawClusterSel_[i][1][a].error * strawClusterSel_[i][1][a].error + SigmaMScat(DZHVIEWS+DZTVIEWS));

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], coord_x, coord_y, trailingpoint, 0, point_errorx, point_errory, 2, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = b;

							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
			for (int b = 0; b < nStrawClustersSel[i][3]; b++) { //y (u,y)

				if (strawClusterSel_[i][3][b].used == 0 or strawClusterSel_[i][1][a].used == 0) {

					float coord_v = strawClusterSel_[i][1][a].coordinate - (Sq2_ * strawClusterSel_[i][3][b].coordinate);
					float coord_u = strawClusterSel_[i][1][a].coordinate;
					float coord_x = (Sq2_ * strawClusterSel_[i][1][a].coordinate) - strawClusterSel_[i][3][b].coordinate;
					float coord_y = strawClusterSel_[i][3][b].coordinate;

					if (strawAcceptance(i, coord_v, coord_u, coord_x, coord_y, 4) == 1) {						

						int flag_accident = 0;

						for (int c = 0; c < nStrawPointsTemp[i]; c++)
						{
							if((strawPointTemp_[i][c].views[1] == a or strawPointTemp_[i][c].views[3] == b) and strawPointTemp_[i][c].nViews>2 and strawPointTemp_[i][c].viewDistance < 2) flag_accident = 1;
						}

						if(flag_accident == 0)
						{
							trailingpoint = (strawClusterSel_[i][1][a].trailing + strawClusterSel_[i][3][b].trailing) / 2;

							float point_errorx = sqrt(strawClusterSel_[i][3][b].error * strawClusterSel_[i][3][b].error + 2 * strawClusterSel_[i][1][a].error * strawClusterSel_[i][1][a].error + SigmaMScat(2*DZHVIEWS+DZTVIEWS));
							float point_errory = sqrt((strawClusterSel_[i][3][b].error*strawClusterSel_[i][3][b].error) + SigmaMScat(2*DZHVIEWS+DZTVIEWS)); //i add the mult-scattering for 4 straw

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], coord_x, coord_y, trailingpoint, 0, point_errorx, point_errory, 2, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[1] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = b;

							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
		}
		for (int a = 0; a < nStrawClustersSel[i][2]; a++) { //x

			for (int b = 0; b < nStrawClustersSel[i][3]; b++) { //y (x,y)

				if (strawClusterSel_[i][3][b].used == 0 or strawClusterSel_[i][2][a].used == 0) {

					float coord_v = (strawClusterSel_[i][2][a].coordinate - strawClusterSel_[i][3][b].coordinate) / Sq2_;
					float coord_u = (strawClusterSel_[i][3][b].coordinate + strawClusterSel_[i][2][a].coordinate) / Sq2_;
					float coord_x = strawClusterSel_[i][2][a].coordinate;
					float coord_y = strawClusterSel_[i][3][b].coordinate;

					if (strawAcceptance(i, coord_v, coord_u, coord_x, coord_y, 5) == 1) {

						int flag_accident = 0;

						for (int c = 0; c < nStrawPointsTemp[i]; c++)
						{
							if((strawPointTemp_[i][c].views[2] == a or strawPointTemp_[i][c].views[3] == b) and strawPointTemp_[i][c].nViews>2 and strawPointTemp_[i][c].viewDistance < 2) flag_accident = 1;
						}

						if(flag_accident == 0)
						{
							trailingpoint = (strawClusterSel_[i][2][a].trailing + strawClusterSel_[i][3][b].trailing) / 2;

							float point_errorx = sqrt((strawClusterSel_[i][2][a].error*strawClusterSel_[i][2][a].error) + SigmaMScat(DZHVIEWS));
							float point_errory = sqrt((strawClusterSel_[i][3][b].error*strawClusterSel_[i][3][b].error) + SigmaMScat(DZHVIEWS)); 

							strawPointTemp_[i][nStrawPointsTemp[i]].setPoint(ChamberZPosition_[i], coord_x, coord_y, trailingpoint, 0, point_errorx, point_errory, 2, 0);

							strawPointTemp_[i][nStrawPointsTemp[i]].views[2] = a;
							strawPointTemp_[i][nStrawPointsTemp[i]].views[3] = b;

							nStrawPointsTemp[i]++;
							if (nStrawPointsTemp[i] >= MAX_N_POINT_TEMP) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}
				}
			}
		}
	}

	////////////////////////////////////////POINT SELECTION/////////////////////////////////////////////////////////
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < nStrawPointsTemp[i]; j++)
		{ 

			int isSharedTight = 0;
			int isSharedLoose = 0;
			
			if(strawPointTemp_[i][j].nViews > 2)
			{ 
				for (int k = 0; k < nStrawPointsTemp[i]; k++)
				{ 
					if (strawPointTemp_[i][k].nViews == 2 or k==j) continue;
					int nshared = 0;

					for( int v=0; v < 4; v++)
					{
						if(strawPointTemp_[i][j].views[v]>-1 and strawPointTemp_[i][k].views[v] == strawPointTemp_[i][j].views[v]) nshared++;
					}
					if(nshared>1 and strawPointTemp_[i][k].viewDistance < strawPointTemp_[i][j].viewDistance)
					{
						isSharedTight = 1;
					}
					if(nshared==1 and strawPointTemp_[i][k].viewDistance < strawPointTemp_[i][j].viewDistance and strawPointTemp_[i][j].viewDistance>4)
					{
						isSharedLoose = 1;
					}
				}
			}

			if(isSharedTight == 0 and isSharedLoose==0 and strawPointTemp_[i][j].trailing > 110 and strawPointTemp_[i][j].trailing < 220)
			{
				strawPointFinal_[i][nStrawPointsFinal[i]].clonePoint(strawPointTemp_[i][j]);
				nStrawPointsFinal[i]++;
				if (nStrawPointsFinal[i] >= MAX_N_POINT_FINAL) {
					return StrawAlgo::abortProcessing(l1Info);
				}
			}
		}
	}

	////////////////////// TRACK RECONSTRUCTION ////////////////////////////////////
	float cda = 0.;

	float mTemp;

	int nCam = 0;
	int qyHist = 0;
	int chkCamera = 0;
	int chkCam = 0;
	int nChkCam = 0;
	int addCam1 = 0;
	int addCam2 = 0;

	int nFirstTrack = 0;
	int nFirstTrackCentrali = 0;
	int tempNHitC = 0;
	int tempNHitL = 0;
	int temp2NHitC = 0;

	int nAddHit1 = 0;
	int nAddHit2 = 0;
	int tempHit = 0;
	int tempCamera = 0;
	int chkHit = 0; 
	float pointMY = 0.0;
	float pointQY = 0.0;

	int nTrack = 0;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < nStrawPointsFinal[i]; j++) //loop over point
		{
			nCam = i;
			for (int a = 0; a < RANGEM; a++)
			{
				mTemp = ((float) a - ((float) RANGEM + 0.1) / 2) * PASSO;
				qy = strawPointFinal_[i][j].y - mTemp * (strawPointFinal_[i][j].z - ZMAGNET); 
				qyHist = qy * RANGEQ / LMAGNET + RANGEQ / 2; 

				if (qyHist > 0 && qyHist < RANGEQ && (hought[a][qyHist] >> 60) < 6) {
					hought[a][qyHist] |= ((long long) (0x3 & nCam) << ((hought[a][qyHist] >> 60) * 2 + 48)); //12 bits from 48 to 59 with the point chamber number (2 bit for chamber)
					hought[a][qyHist] |= ((((long long) (0xFF & (j))) << ((hought[a][qyHist] >> 60) * 8)) & 0XFFFFFFFFFFFF); //the first 48 bits with up to 6 point of 8 bits ( 255 )
					hought[a][qyHist] += ((long long) 1 << 60); //the 4 most significant bits with the number of points
				}
			}
		}
	}

	nFirstTrack = 0;

	for (int a = 0; a < RANGEM; a++) {
		for (int b = 0; b < RANGEQ; b++) {
			if ((hought[a][b] >> 60) > 1) //looking for bin with at least 2 points
					{
				chkCam = 0;
				nChkCam = 4;
				addCam1 = -1;
				addCam2 = -1;
				chkCamera = 0;

				//verificare
				nFirstTrack = 0;
				nFirstTrackCentrali = 0;
				tempNHitC = 0;
				tempNHitL = 0;
				nAddHit1 = 0;

				nAddHit1 = 0;
				nAddHit2 = 0;
				tempHit = 0;
				tempCamera = 0;
				chkHit = 0; 
				pointMY = 0.0;
				pointQY = 0.0;

				for (int c = 0; c < (int) (hought[a][b] >> 60); c++) { // check if there are at least 2 hits belonging to different chambers
					chkCam |= 1 << (0X3 & (hought[a][b] >> (48 + 2 * c)));
				}
				for (int d = 0; d < 4; d++) {
					if ((0X1 & (chkCam >> d)) == 0) {
						nChkCam--;

						if (addCam1 == -1)
							addCam1 = d;
						else
							addCam2 = d;
					}
				}
				if (nChkCam > 1) {
					for (int d = 0; d < (int) (hought[a][b] >> 60); d++) {

						if (((int) pow(2, (int) (0X3 & (hought[a][b] >> (48 + 2 * d)))) & chkCamera) == 0) {

							for (int j = 0; j < nFirstTrack + 1; j++) {
								strawFirstTempTrk_[j].hitc[tempNHitC] = (int) (0XFF & (hought[a][b] >> (8 * d)));
								strawFirstTempTrk_[j].camerec[tempNHitC] = (int) (0X3 & (hought[a][b] >> (48 + 2 * d)));

								chkCamera |= (int) pow(2, (int) (0X3 & (hought[a][b] >> (48 + 2 * d))));

								strawFirstTempTrk_[j].ncentrali = tempNHitC + 1;
								strawFirstTempTrk_[j].nlaterali = 0;
							}
							tempNHitC++;
							if (tempNHitC >= MAX_N_HIT_C) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						} else 
						{
							temp2NHitC = 0;
							nFirstTrack++;
							if (nFirstTrack >= MAX_N_FIRST_TEMP_TRACK) {
								return StrawAlgo::abortProcessing(l1Info);
							}
							for (int j = 0; j < tempNHitC; j++) {

								if (strawFirstTempTrk_[nFirstTrack - 1].camerec[j] != (int) (0X3 & (hought[a][b] >> (48 + 2 * d)))) {
									strawFirstTempTrk_[nFirstTrack].hitc[temp2NHitC] = strawFirstTempTrk_[nFirstTrack - 1].hitc[j];
									strawFirstTempTrk_[nFirstTrack].camerec[temp2NHitC] = strawFirstTempTrk_[nFirstTrack - 1].camerec[j];
								} else {
									if (strawPointFinal_[(int) (0X3 & (hought[a][b] >> (48 + 2 * d)))][(int) (0XFF
											& (hought[a][b] >> (8 * d)))].nViews
											> strawPointFinal_[strawFirstTempTrk_[nFirstTrack - 1].camerec[temp2NHitC]][strawFirstTempTrk_[nFirstTrack
													- 1].hitc[temp2NHitC]].nViews) {
										nFirstTrack--;
										strawFirstTempTrk_[nFirstTrack].hitc[temp2NHitC] = (int) (0XFF & (hought[a][b] >> (8 * d)));
										strawFirstTempTrk_[nFirstTrack].camerec[temp2NHitC] = (int) (0X3 & (hought[a][b] >> (48 + 2 * d)));
									} else if (strawPointFinal_[(int) (0X3 & (hought[a][b] >> (48 + 2 * d)))][(int) (0XFF
											& (hought[a][b] >> (8 * d)))].nViews
											== strawPointFinal_[strawFirstTempTrk_[nFirstTrack - 1].camerec[temp2NHitC]][strawFirstTempTrk_[nFirstTrack
													- 1].hitc[temp2NHitC]].nViews) {
										strawFirstTempTrk_[nFirstTrack].hitc[temp2NHitC] = (int) (0XFF & (hought[a][b] >> (8 * d)));
										strawFirstTempTrk_[nFirstTrack].camerec[temp2NHitC] = (int) (0X3 & (hought[a][b] >> (48 + 2 * d)));
									} else {
										nFirstTrack--;
									}
								}

								temp2NHitC++;
								if (temp2NHitC >= MAX_N_HIT_C) {
									return StrawAlgo::abortProcessing(l1Info);
								}
							}
							strawFirstTempTrk_[nFirstTrack].ncentrali = temp2NHitC;
							strawFirstTempTrk_[nFirstTrack].nlaterali = 0;
						}
					}
					nFirstTrackCentrali = nFirstTrack;

					if (tempNHitC > 1 && tempNHitC < 4) 
					{
						nAddHit1 = 0;
						nAddHit2 = 0;

						for (int h = -1; h < 2; h++) {
							for (int k = -1; k < 2; k++) {
								if (a + h > -1 && a + h < RANGEM && b + k > -1 && b + k < 200) {
									for (int l = 0; l < (hought[a + h][b + k] >> 60); l++) {
										tempHit = (int) (0XFF & (hought[a + h][b + k] >> (8 * l)));
										tempCamera = (int) (0X3 & (hought[a + h][b + k] >> (48 + 2 * l)));
										chkHit = 0;

										if (tempCamera == addCam1) {
											//look if there already are the hit in addhit1[]

											for (int d = 0; d < nAddHit1; d++) {
												if (addhit1[d] == tempHit) {
													chkHit = 1;
												}
											}

											if (chkHit == 0) {
												addhit1[nAddHit1] = tempHit;
												nAddHit1++;
												if (nAddHit1 >= MAX_N_ADD_HIT) {
													return StrawAlgo::abortProcessing(l1Info);
												}
											}
										}
										if (tempCamera == addCam2) {
											for (int d = 0; d < nAddHit2; d++) {
												if (addhit2[d] == tempHit) {
													chkHit = 1;
												}
											}

											if (chkHit == 0) {
												addhit2[nAddHit2] = tempHit;
												nAddHit2++;
												if (nAddHit2 >= MAX_N_ADD_HIT) {
													return StrawAlgo::abortProcessing(l1Info);
												}
											}
										}
									}
								}
							}
						}
						if (nAddHit1 > 0) {
							for (int j = 0; j < nFirstTrackCentrali + 1; j++) {
								strawFirstTempTrk_[j].hitl[tempNHitL] = addhit1[0];
								strawFirstTempTrk_[j].camerel[tempNHitL] = addCam1;
								strawFirstTempTrk_[j].nlaterali = tempNHitL + 1;
							}

							for (int d = 1; d < nAddHit1; d++) {
								for (int j = 0; j < nFirstTrackCentrali + 1; j++) {
									nFirstTrack++;
									if (nFirstTrack >= MAX_N_FIRST_TEMP_TRACK) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawFirstTempTrk_[nFirstTrack].copyTrack(strawFirstTempTrk_[j]);
									strawFirstTempTrk_[nFirstTrack].hitl[tempNHitL] = addhit1[d];
								}
							}
							tempNHitL++;
							if (tempNHitL >= MAX_N_HIT_L) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
						nFirstTrackCentrali = nFirstTrack; //sono aumentati in seguito all'addhit1

						if (nAddHit2 > 0) {
							for (int j = 0; j < nFirstTrackCentrali + 1; j++) {
								strawFirstTempTrk_[j].hitl[tempNHitL] = addhit2[0];
								strawFirstTempTrk_[j].camerel[tempNHitL] = addCam2;
								strawFirstTempTrk_[j].nlaterali = tempNHitL + 1;
							}

							for (int d = 1; d < nAddHit2; d++) {
								for (int j = 0; j < nFirstTrackCentrali + 1; j++) {
									nFirstTrack++;
									if (nFirstTrack >= MAX_N_FIRST_TEMP_TRACK) {
										return StrawAlgo::abortProcessing(l1Info);
									}
									strawFirstTempTrk_[nFirstTrack].copyTrack(strawFirstTempTrk_[j]);
									strawFirstTempTrk_[nFirstTrack].hitl[tempNHitL] = addhit2[d];
								}
							}
							tempNHitL++;
							if (tempNHitL >= MAX_N_HIT_L) {
								return StrawAlgo::abortProcessing(l1Info);
							}
						}
					}

					//now I have all the tracklet find in the bin, I have to select only the real one

					pointMY = ((float) a - ((float) RANGEM + 0.1) / 2) * PASSO;
					pointQY = b * LMAGNET / RANGEQ - LMAGNET / 2;

					for (int j = 0; j < nFirstTrack + 1; j++) { //parte X

						float x0 = 0.0;
						float x1 = 0.0;
						float x2 = 0.0;
						float x3 = 0.0;
						float z0 = 0.0;
						float z1 = 0.0;
						float z2 = 0.0;
						float z3 = 0.0;
						float q01 = 0.0;
						float q23 = 0.0;
						float trailingTemp = 0.0;

						if (strawFirstTempTrk_[j].ncentrali + strawFirstTempTrk_[j].nlaterali > 2) {
							for (int z = 0; z < tempNHitC; z++) {
								trailingTemp += strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].trailing;

								if (strawFirstTempTrk_[j].camerec[z] == 0) {
									x0 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].x;
									z0 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].z - ZMAGNET;
								}
								if (strawFirstTempTrk_[j].camerec[z] == 1) {
									x1 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].x;
									z1 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].z - ZMAGNET;
								}
								if (strawFirstTempTrk_[j].camerec[z] == 2) {
									x2 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].x;
									z2 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].z - ZMAGNET;
								}
								if (strawFirstTempTrk_[j].camerec[z] == 3) {
									x3 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].x;
									z3 = strawPointFinal_[strawFirstTempTrk_[j].camerec[z]][strawFirstTempTrk_[j].hitc[z]].z - ZMAGNET;
								}
							}
							for (int z = 0; z < tempNHitL; z++) {
								trailingTemp += strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].trailing;

								if (strawFirstTempTrk_[j].camerel[z] == 0) {
									x0 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].x;
									z0 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].z - ZMAGNET;
								}
								if (strawFirstTempTrk_[j].camerel[z] == 1) {
									x1 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].x;
									z1 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].z - ZMAGNET;
								}
								if (strawFirstTempTrk_[j].camerel[z] == 2) {
									x2 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].x;
									z2 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].z - ZMAGNET;
								}
								if (strawFirstTempTrk_[j].camerel[z] == 3) {
									x3 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].x;
									z3 = strawPointFinal_[strawFirstTempTrk_[j].camerel[z]][strawFirstTempTrk_[j].hitl[z]].z - ZMAGNET;
								}
							}

							if (z0 == 0 || z1 == 0) {

								q23 = x2 - z2 * (x3 - x2) / (z3 - z2);
								q01 = q23;

								strawFirstTempTrk_[j].m2x = (x3 - x2) / (z3 - z2);
								if (z0 == 0) {
									strawFirstTempTrk_[j].m1x = (x1 - q01) / z1;
								} else {
									strawFirstTempTrk_[j].m1x = (x0 - q01) / z0;
								}
							} else if (z2 == 0 || z3 == 0) {
								q01 = x0 - z0 * (x1 - x0) / (z1 - z0);
								q23 = q01;

								strawFirstTempTrk_[j].m1x = (x1 - x0) / (z1 - z0);
								if (z2 == 0) {
									strawFirstTempTrk_[j].m2x = (x3 - q23) / z3;
								} else {
									strawFirstTempTrk_[j].m2x = (x2 - q23) / z2;
								}
							} else {
								q01 = x0 - z0 * (x1 - x0) / (z1 - z0);
								q23 = x2 - z2 * (x3 - x2) / (z3 - z2);
								strawFirstTempTrk_[j].m1x = (x1 - x0) / (z1 - z0);
								strawFirstTempTrk_[j].m2x = (x3 - x2) / (z3 - z2);
							}

							//dqx = q23 - q01;

							strawFirstTempTrk_[j].q1x = q01;
							strawFirstTempTrk_[j].q2x = q23;
							strawFirstTempTrk_[j].pz = 270.0 / (fabs(strawFirstTempTrk_[j].m2x - strawFirstTempTrk_[j].m1x));

							strawFirstTempTrk_[j].my = pointMY;
							strawFirstTempTrk_[j].qy = pointQY;
							strawFirstTempTrk_[j].trailing = trailingTemp / (tempNHitC + tempNHitL);

							qTrack.setPoint(0.0, strawFirstTempTrk_[j].q1x, strawFirstTempTrk_[j].qy, 0.0, 0.0, 0.0, 0.0, 0, 0);
							mTrack.setPoint(1.0, strawFirstTempTrk_[j].m1x, strawFirstTempTrk_[j].my, 0.0, 0.0, 0.0, 0.0, 0, 0);

							cdaVertex(QBeam_, qTrack, MBeam_, mTrack, cda, vertex);

							strawFirstTempTrk_[j].zvertex = vertex.z;
							strawFirstTempTrk_[j].cda = cda;
							strawFirstTempTrk_[j].dQxPrefit = q23 - q01;

							if (strawFirstTempTrk_[j].pz > 3000 && strawFirstTempTrk_[j].pz < 200000 && strawFirstTempTrk_[j].my < 0.020
									&& strawFirstTempTrk_[j].m1x < 0.020) {
								strawTempTrk_[nTrack].copyTrack(strawFirstTempTrk_[j]);
								nTrack++;
								if (nTrack >= MAX_N_TRACKS) {
									return StrawAlgo::abortProcessing(l1Info);
								}
							}
						}
					}

				}

			}
		}
	}

	/////////////////////////// Tentativo Kalman Filter Fit ///////////////////////////////
	for (int e = 0; e < nTrack; e++) {

		float momentum = strawTempTrk_[e].pz/1000.0;

		float PosCamY[4] = {-999999,-999999,-999999,-999999};
		float PosCamX[4] = {-999999,-999999,-999999,-999999};
		float ErrorPosY[4] = {-999999,-999999,-999999,-999999};
		float ErrorPosX[4] = {-999999,-999999,-999999,-999999};
		float ChamberZ[4] = {ZCHAMBER0,ZCHAMBER1,ZCHAMBER2,ZCHAMBER3};
		
		for(int l=0; l < strawTempTrk_[e].nlaterali; l++)
		{
			if(strawTempTrk_[e].camerel[l] >= 0 and strawTempTrk_[e].camerel[l] <= 3)
			{
				int index_caml = strawTempTrk_[e].camerel[l];
				PosCamX[index_caml] = strawPointFinal_[index_caml][strawTempTrk_[e].hitl[l]].x;
				PosCamY[index_caml] = strawPointFinal_[index_caml][strawTempTrk_[e].hitl[l]].y;
				ErrorPosX[index_caml] = strawPointFinal_[index_caml][strawTempTrk_[e].hitl[l]].errorx;
				ErrorPosY[index_caml] = strawPointFinal_[index_caml][strawTempTrk_[e].hitl[l]].errory;
			}
		}
		for(int c=0; c < strawTempTrk_[e].ncentrali; c++)
		{			
			if(strawTempTrk_[e].camerec[c] >= 0 and strawTempTrk_[e].camerec[c] <= 3)
			{
				int index_camc = strawTempTrk_[e].camerec[c];
				PosCamX[index_camc] = strawPointFinal_[index_camc][strawTempTrk_[e].hitc[c]].x;
				PosCamY[index_camc] = strawPointFinal_[index_camc][strawTempTrk_[e].hitc[c]].y;
				ErrorPosX[index_camc] = strawPointFinal_[index_camc][strawTempTrk_[e].hitc[c]].errorx;
				ErrorPosY[index_camc] = strawPointFinal_[index_camc][strawTempTrk_[e].hitc[c]].errory;
			}
		}

		int nchamber = 0;
		float KalmanChi2X = 0;
		float KalmanChi2Xpos = 0;
		float KalmanChi2Xneg = 0;
		float KalmanChi2Y = 0;

		for (int c = 0; c < 4; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = strawTempTrk_[e].my * (ChamberZ[c] - ZMAGNET) + strawTempTrk_[e].qy;
				float DistRealKalman = PosAtChamber - PosCamY[c];
				KalmanChi2Y += (DistRealKalman*DistRealKalman)/CHI2ERR;
				nchamber++;
			}
		}
		for (int c = 0; c < 2; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = strawTempTrk_[e].m1x * (ChamberZ[c] - ZMAGNET) + strawTempTrk_[e].q1x;
				float DistRealKalman = PosAtChamber - PosCamX[c];
				KalmanChi2X += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}
		for (int c = 2; c < 4; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = strawTempTrk_[e].m2x * (ChamberZ[c] - ZMAGNET) + strawTempTrk_[e].q2x;
				float DistRealKalman = PosAtChamber - PosCamX[c];
				KalmanChi2X += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}

		KalmanChi2Y = KalmanChi2Y/nchamber;
		KalmanChi2X = KalmanChi2X/nchamber;

		float PosCamTemp = -999999;
		float ZTemp = -999999;
		float dist_point = -999999;
		float res_point_angle = -999999;
		float err_measure_angle = -999999;
		float err_measure_point = -999999;
		float gain_angle = -999999;
		float gain_point = -999999;
		float angle_errorTemp = -999999;
		float point_errorTemp = -999999;
		float angle_chamber_Est = -999999;
		float point_chamber_Est = -999999;
		//float AngleY_kaman = -999999; remove
		//float QY_kaman = -999999;//remove
		float new_q1x_pos = -999999;
		float new_q1x_neg = -999999;
		float new_m1x_pos = -999999;
		float new_m1x_neg = -999999;

		ZTemp = ZMAGNET;
		PosCamTemp = strawTempTrk_[e].qy;
		angle_errorTemp = SIGMASLOPEH;
		point_errorTemp = SIGMAPOINTH;
		angle_chamber_Est = strawTempTrk_[e].my;

		for (int s = 0; s < 4; ++s)
		{
			if(PosCamY[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosY[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp); //projection new point with old angle
				float AngleCam = (PosCamY[s] - PosCamTemp)/(ChamberZ[s] - ZTemp); //misured angle

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam); //new angle
				float point_chamber = PosCamTemp + angle_chamber * (ChamberZ[s] - ZTemp); //new projection with new angle
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				//float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;

				ZTemp = ChamberZ[s];

			}
		}

		float newqy = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		ZTemp = ZMAGNET;
		PosCamTemp = newqy;


		for (int s = 3; s > -1; --s)
		{
			if(PosCamY[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosY[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp); //projection new point with old angle

				float AngleCam = (PosCamY[s] - PosCamTemp)/(ChamberZ[s] - ZTemp); //misured angle

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);//new angle
				float point_chamber = PosCamTemp + angle_chamber * (ChamberZ[s] - ZTemp); //new projection with new angle
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				//float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;

				ZTemp = ChamberZ[s];

			}
		}
		
		newqy = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);

		KalmanChi2Y = 0;

		for (int c = 0; c < 4; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = angle_chamber_Est * (ChamberZ[c] - ZMAGNET) + newqy;
				float DistRealKalman = PosAtChamber - PosCamY[c];
				KalmanChi2Y += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}
		KalmanChi2Y = KalmanChi2Y/nchamber;

		strawTempTrk_[e].qy = newqy;
		strawTempTrk_[e].my = angle_chamber_Est;
		strawTempTrk_[e].chi2y = KalmanChi2Y;

		/////////////////////////
		///// x part kalman
		/////////////////////////

		angle_chamber_Est = strawTempTrk_[e].m1x;
		ZTemp = ZMAGNET;
		dist_point = ZCHAMBER1 - ZCHAMBER0;
		angle_errorTemp =  sqrt(2*RESPOINT*RESPOINT/(dist_point*dist_point)+ (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
		point_errorTemp = sqrt(RESPOINT*RESPOINT + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));
		PosCamTemp = (strawTempTrk_[e].q1x + strawTempTrk_[e].q2x)/2.0;

		for (int s = 0; s < 2; ++s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];

			}
		}

		float new_q1x = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		float new_m1x = angle_chamber_Est;

		//////i try both the negative and positive charge
		
		////negative charge////

		angle_chamber_Est = new_m1x + 270/strawTempTrk_[e].pz;
		ZTemp = ZMAGNET;
		PosCamTemp = new_q1x;
		float angle_error_mag = angle_errorTemp;
		float point_error_mag = point_errorTemp;

		for (int s = 2; s < 4; ++s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];
			}
		}

		float new_q2x_neg = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);		
		float new_m2x_neg = angle_chamber_Est;
		ZTemp = ZMAGNET;
		PosCamTemp = new_q2x_neg;
		float new_momentum_neg =  270.0 / (fabs(new_m2x_neg - new_m1x));

		for (int s = 3; s > 1; --s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];

			}
		}

		new_q2x_neg = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		new_m2x_neg = angle_chamber_Est;
		ZTemp = ZMAGNET;
		angle_chamber_Est = new_m2x_neg - 270/new_momentum_neg;
		PosCamTemp = new_q2x_neg;

		for (int s = 1; s > -1; --s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];
			}
		}


		new_q1x_neg = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		new_m1x_neg = angle_chamber_Est;
		new_momentum_neg =  270.0 / (fabs(new_m2x_neg - new_m1x_neg));

		KalmanChi2Xneg = 0;
		for (int c = 0; c < 2; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = new_m1x_neg * (ChamberZ[c] - ZMAGNET) + new_q1x_neg;
				float DistRealKalman = PosAtChamber - PosCamX[c];
				KalmanChi2Xneg += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}
		for (int c = 2; c < 4; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = new_m2x_neg * (ChamberZ[c] - ZMAGNET) + new_q2x_neg;
				float DistRealKalman = PosAtChamber - PosCamX[c];
				KalmanChi2Xneg += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}
		KalmanChi2Xneg = KalmanChi2Xneg/nchamber;

		/////////////// pos charge //////////////////7
		angle_chamber_Est = new_m1x - 270/strawTempTrk_[e].pz;
		ZTemp = ZMAGNET;
		PosCamTemp = new_q1x;
		angle_errorTemp = angle_error_mag;
		point_errorTemp = point_error_mag;

		for (int s = 2; s < 4; ++s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];
			}
		}

		float new_q2x_pos = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		float new_m2x_pos = angle_chamber_Est;
		ZTemp = ZMAGNET;
		PosCamTemp = new_q2x_pos;
		float new_momentum_pos =  270.0 / (fabs(new_m2x_pos - new_m1x));

		for (int s = 3; s > 1; --s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];
			}
		}

		new_q2x_pos = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		new_m2x_pos = angle_chamber_Est;
		ZTemp = ZMAGNET;
		angle_chamber_Est = new_m2x_pos + 270/new_momentum_pos;
		PosCamTemp = new_q2x_pos;

		for (int s = 1; s > -1; --s)
		{
			if(PosCamX[s] != -999999)
			{
				dist_point = fabs(ChamberZ[s] - ZTemp);
				res_point_angle = ErrorPosX[s]/(dist_point);
				err_measure_angle = sqrt(res_point_angle*res_point_angle + (2*SIGMAMULTSCATTERING/momentum)*(2*SIGMAMULTSCATTERING/momentum));
				err_measure_point = sqrt(ErrorPosX[s]*ErrorPosX[s] + (2*SIGMAMULTSCATTERING*dist_point/momentum)*(2*SIGMAMULTSCATTERING*dist_point/momentum));

				gain_angle = gain(angle_errorTemp, err_measure_angle);
				gain_point = gain(point_errorTemp, err_measure_point);

				point_chamber_Est = PosCamTemp + angle_chamber_Est * (ChamberZ[s] - ZTemp);

				float AngleCam = (PosCamX[s] - PosCamTemp)/(ChamberZ[s] - ZTemp);

				float angle_chamber = newEstimate(angle_chamber_Est, gain_angle, AngleCam);
				float point_chamber = newEstimate(point_chamber_Est, gain_point, PosCamX[s]);
				angle_chamber_Est = angle_chamber;
				PosCamTemp = point_chamber;

				float angle_error = newError(angle_errorTemp, gain_angle);
				float point_error = newError(point_errorTemp, gain_point);
				angle_errorTemp = angle_error;
				point_errorTemp = point_error;

				ZTemp = ChamberZ[s];
			}
		}

		new_q1x_pos = PosCamTemp - angle_chamber_Est * (ZTemp - ZMAGNET);
		new_m1x_pos = angle_chamber_Est;
		new_momentum_pos =  270.0 / (fabs(new_m2x_pos - new_m1x_pos));

		for (int c = 0; c < 2; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = new_m1x_pos * (ChamberZ[c] - ZMAGNET) + new_q1x_pos;
				float DistRealKalman = PosAtChamber - PosCamX[c];
				KalmanChi2Xpos += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}
		for (int c = 2; c < 4; ++c)
		{
			if(PosCamY[c] != -999999)
			{
				float PosAtChamber = new_m2x_pos * (ChamberZ[c] - ZMAGNET) + new_q2x_pos;
				float DistRealKalman = PosAtChamber - PosCamX[c];
				KalmanChi2Xpos += (DistRealKalman*DistRealKalman)/CHI2ERR;
			}
		}
		KalmanChi2Xpos = KalmanChi2Xpos/nchamber;
		
		if(KalmanChi2Xpos < KalmanChi2Xneg)
		{
			strawTempTrk_[e].m1x = new_m1x_pos;
			strawTempTrk_[e].q1x = new_q1x_pos;
			strawTempTrk_[e].m2x = new_m2x_pos;
			strawTempTrk_[e].q2x = new_q2x_pos;
			strawTempTrk_[e].chi2x = KalmanChi2Xpos;
			strawTempTrk_[e].pz = new_momentum_pos;
		}
		else
		{
			strawTempTrk_[e].m1x = new_m1x_neg;
			strawTempTrk_[e].q1x = new_q1x_neg;
			strawTempTrk_[e].m2x = new_m2x_neg;
			strawTempTrk_[e].q2x = new_q2x_neg;
			strawTempTrk_[e].chi2x = KalmanChi2Xneg;
			strawTempTrk_[e].pz = new_momentum_neg;		
		}

		qTrack.setPoint(0.0, strawTempTrk_[e].q1x, strawTempTrk_[e].qy, 0.0, 0.0, 0.0, 0.0, 0, 0);
		mTrack.setPoint(1.0, strawTempTrk_[e].m1x, strawTempTrk_[e].my, 0.0, 0.0, 0.0, 0.0, 0, 0);

		cdaVertex(QBeam_, qTrack, MBeam_, mTrack, cda, vertex);

		strawTempTrk_[e].zvertex = vertex.z;
		strawTempTrk_[e].cda = cda;

		int nCam = strawTempTrk_[e].ncentrali + strawTempTrk_[e].nlaterali;

		if(nCam == 3) strawTempTrk_[e].chi2x = 5.0; // 3.0;

	}

	//////////fine kalman filter
	nTrackIntermedie = 0;
	nTrackFinal = 0;
	nTrackFinalExo = 0;

///////////////TRACK SELECTION////////////
//////////////////////////// first track selection  /////////////////////////////////////////////////
	for (int e = 0; e < nTrack; e++) {
		int isShared = 0;

		for (int c = 0; c < 4; ++c)
		{
			ChambCondivise[c] = 0;
		}
		nChambCondivise = 0;

		trackIntermedieTemp.copyTrack(strawTempTrk_[e]);

	 	for (int f = 0; f < nTrack; f++)
		{
			if (f==e) continue;
			tempCondivise = 0;

			for (int g = 0; g < trackIntermedieTemp.ncentrali; g++) {
				for (int h = 0; h < strawTempTrk_[f].ncentrali; h++) {

					if (trackIntermedieTemp.hitc[g] == strawTempTrk_[f].hitc[h]
							&& trackIntermedieTemp.camerec[g] == strawTempTrk_[f].camerec[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
						tempCondivise++;
					}
				}
				for (int h = 0; h < strawTempTrk_[f].nlaterali; h++) {
					if (trackIntermedieTemp.hitc[g] == strawTempTrk_[f].hitl[h]
							&& trackIntermedieTemp.camerec[g] == strawTempTrk_[f].camerel[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
						tempCondivise++;
					}
				}
			}

			for (int g = 0; g < trackIntermedieTemp.nlaterali; g++) {
				for (int h = 0; h < strawTempTrk_[f].ncentrali; h++) {

					if (trackIntermedieTemp.hitl[g] == strawTempTrk_[f].hitc[h]
							&& trackIntermedieTemp.camerel[g] == strawTempTrk_[f].camerec[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
						tempCondivise++;
					}
				}
				for (int h = 0; h < strawTempTrk_[f].nlaterali; h++) {

					if (trackIntermedieTemp.hitl[g] == strawTempTrk_[f].hitl[h]
							&& trackIntermedieTemp.camerel[g] == strawTempTrk_[f].camerel[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
						tempCondivise++;
					}
				}
			}

			float Dchi2Sum = trackIntermedieTemp.chi2y + trackIntermedieTemp.chi2x - strawTempTrk_[f].chi2y - strawTempTrk_[f].chi2x;

			if ((tempCondivise > 1 and Dchi2Sum > 0) or (Dchi2Sum == 0 and f < e))
			{
				isShared = 1;
			}
		}

		if(isShared == 0) 
		{
			strawTrkIntermedie_[nTrackIntermedie].copyTrack(trackIntermedieTemp);
			nTrackIntermedie++;

			if (nTrackIntermedie >= MAX_N_TRACK_INTERMEDIE) {
				return StrawAlgo::abortProcessing(l1Info);
			}
		}
	}

//////////////////////////// second track selection: normal decay flux  /////////////////////////////////////////////////
	for (int e = 0; e < nTrackIntermedie; e++) {

		if (strawTrkIntermedie_[e].zvertex > 190000) continue;

		int nCamE = strawTrkIntermedie_[e].ncentrali + strawTrkIntermedie_[e].nlaterali;

		for (int c = 0; c < 4; ++c)
		{
			ChambCondivise[c] = 0;
		}
		nChambCondivise = 0;
		trackIntermedieTemp.copyTrack(strawTrkIntermedie_[e]);

	 	for (int f = 0; f < nTrackIntermedie; f++)
		{
			if (f==e or strawTrkIntermedie_[f].zvertex > 190000) continue;
			
			for (int g = 0; g < trackIntermedieTemp.ncentrali; g++) {
				for (int h = 0; h < strawTrkIntermedie_[f].ncentrali; h++) {

					if (trackIntermedieTemp.hitc[g] == strawTrkIntermedie_[f].hitc[h]
							&& trackIntermedieTemp.camerec[g] == strawTrkIntermedie_[f].camerec[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
				for (int h = 0; h < strawTrkIntermedie_[f].nlaterali; h++) {
					if (trackIntermedieTemp.hitc[g] == strawTrkIntermedie_[f].hitl[h]
							&& trackIntermedieTemp.camerec[g] == strawTrkIntermedie_[f].camerel[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
			}

			for (int g = 0; g < trackIntermedieTemp.nlaterali; g++) {
				for (int h = 0; h < strawTrkIntermedie_[f].ncentrali; h++) {

					if (trackIntermedieTemp.hitl[g] == strawTrkIntermedie_[f].hitc[h]
							&& trackIntermedieTemp.camerel[g] == strawTrkIntermedie_[f].camerec[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
				for (int h = 0; h < strawTrkIntermedie_[f].nlaterali; h++) {

					if (trackIntermedieTemp.hitl[g] == strawTrkIntermedie_[f].hitl[h]
							&& trackIntermedieTemp.camerel[g] == strawTrkIntermedie_[f].camerel[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
			}
		}

		float Dchi2Sum = trackIntermedieTemp.chi2y + trackIntermedieTemp.chi2x;

		for (int c = 0; c < 4; ++c)
		{
			nChambCondivise += ChambCondivise[c];
		}


		if((nCamE > (nChambCondivise + 1)) or Dchi2Sum < 10)
		{

			strawTrkFinal_[nTrackFinal].copyTrack(trackIntermedieTemp);
			nTrackFinal++;

			if (nTrackFinal >= MAX_N_TRACK_FINAL) {
				return StrawAlgo::abortProcessing(l1Info);
			}
		}
	}

//////////////////////////// second track selection: exo decay flux  /////////////////////////////////////////////////

	for (int e = 0; e < nTrackIntermedie; e++) {
		int nCamE = strawTrkIntermedie_[e].ncentrali + strawTrkIntermedie_[e].nlaterali;

		for (int c = 0; c < 4; ++c)
		{
			ChambCondivise[c] = 0;
		}
		nChambCondivise = 0;

		trackIntermedieTemp.copyTrack(strawTrkIntermedie_[e]);

	 	for (int f = 0; f < nTrackIntermedie; f++)
		{
			if (f==e) continue;
			
			for (int g = 0; g < trackIntermedieTemp.ncentrali; g++) {
				for (int h = 0; h < strawTrkIntermedie_[f].ncentrali; h++) {

					if (trackIntermedieTemp.hitc[g] == strawTrkIntermedie_[f].hitc[h]
							&& trackIntermedieTemp.camerec[g] == strawTrkIntermedie_[f].camerec[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
				for (int h = 0; h < strawTrkIntermedie_[f].nlaterali; h++) {
					if (trackIntermedieTemp.hitc[g] == strawTrkIntermedie_[f].hitl[h]
							&& trackIntermedieTemp.camerec[g] == strawTrkIntermedie_[f].camerel[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
			}

			for (int g = 0; g < trackIntermedieTemp.nlaterali; g++) {
				for (int h = 0; h < strawTrkIntermedie_[f].ncentrali; h++) {

					if (trackIntermedieTemp.hitl[g] == strawTrkIntermedie_[f].hitc[h]
							&& trackIntermedieTemp.camerel[g] == strawTrkIntermedie_[f].camerec[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
				for (int h = 0; h < strawTrkIntermedie_[f].nlaterali; h++) {

					if (trackIntermedieTemp.hitl[g] == strawTrkIntermedie_[f].hitl[h]
							&& trackIntermedieTemp.camerel[g] == strawTrkIntermedie_[f].camerel[h]) {
						ChambCondivise[trackIntermedieTemp.camerec[g]]=1;
					}
				}
			}
		}

		float Dchi2Sum = trackIntermedieTemp.chi2y + trackIntermedieTemp.chi2x;

		for (int c = 0; c < 4; ++c)
		{
			nChambCondivise += ChambCondivise[c];
		}


		if((nCamE > (nChambCondivise + 1)) or Dchi2Sum < 10)
		{

			strawTrkFinalExo_[nTrackFinalExo].copyTrack(trackIntermedieTemp);
			nTrackFinalExo++;

			if (nTrackFinalExo >= MAX_N_TRACK_FINAL) {
				return StrawAlgo::abortProcessing(l1Info);
			}
		}
	}

	Point mTrack1;
	Point qTrack1;
	Point mTrack2;
	Point qTrack2;

	float distanceToBeamLine = 0.;
	float distance_temp = 0.;

	float DZMUV3 = ZMUV3-ZMAGNET;

	for (int e = 0; e < nTrackFinal; e++) {
		// strawTrkFinal_[e].printTrack();

		flagL1Limit[e] = 0;

		if (e < 4) {
			l1Info->setL1StrawTrackP(e, strawTrkFinal_[e].pz);
			l1Info->setL1StrawTrackVz(e, strawTrkFinal_[e].zvertex);
		}

		if (strawTrkFinal_[e].zvertex > -100000 && strawTrkFinal_[e].zvertex < 180000 && strawTrkFinal_[e].cda < 200
				&& strawTrkFinal_[e].pz < AlgoOnlinePNNMomentumCut_[l0MaskID]) {
			flagL1Limit[e]++;
		}

		float PosXatMUV3 = strawTrkFinal_[e].m2x * DZMUV3 + strawTrkFinal_[e].q2x;
		float PosYatMUV3 = strawTrkFinal_[e].my * DZMUV3 + strawTrkFinal_[e].qy;
		float RatMUV3 = sqrt(PosXatMUV3*PosXatMUV3+PosYatMUV3*PosYatMUV3);

		if(RatMUV3>RMUV3MIN && fabs(PosXatMUV3)<RMUV3MAX+0.5*MUV3GAPWIDTH && fabs(PosYatMUV3)<RMUV3MAX)
		{
			flagL1MUV3[e]++;
		}

		if(RatMUV3>RMUV3MINTIGHT && fabs(PosXatMUV3)<RMUV3MAXTIGHT+0.5*MUV3GAPWIDTH && fabs(PosYatMUV3)<RMUV3MAXTIGHT)
		{
			flagL1MUV3Tight[e]++;
		}

		qTrack1.setPoint(0.0, strawTrkFinal_[e].q1x, strawTrkFinal_[e].qy, 0.0, 0.0, 0.0, 0.0, 0, 0);
		mTrack1.setPoint(1.0, strawTrkFinal_[e].m1x, strawTrkFinal_[e].my, 0.0, 0.0, 0.0, 0.0, 0, 0);

		for (int f = e + 1; f < nTrackFinal; f++) {
			qTrack2.setPoint(0.0, strawTrkFinal_[f].q1x, strawTrkFinal_[f].qy, 0.0, 0.0, 0.0, 0.0, 0, 0);
			mTrack2.setPoint(1.0, strawTrkFinal_[f].m1x, strawTrkFinal_[f].my, 0.0, 0.0, 0.0, 0.0, 0, 0);

			cdaVertex(qTrack1, qTrack2, mTrack1, mTrack2, cda, vertex);
			if (cda < 30) {
				flagL1Three[e]++;
				flagL1Three[f]++;
			}

		}

		if (strawTrkFinal_[e].m1x - strawTrkFinal_[e].m2x < 0) {
			flagL1Exotic = 1;
		}

		if (strawTrkFinal_[e].m1x - strawTrkFinal_[e].m2x < 0 && strawTrkFinal_[e].zvertex > 80000 && strawTrkFinal_[e].cda < 500
				&& strawTrkFinal_[e].pz < 65000) {
			flagL1ExoticTight = 1;		

			float PeX = strawTrkFinal_[e].pz * strawTrkFinal_[e].m1x;
			float PeY = strawTrkFinal_[e].pz * strawTrkFinal_[e].my;
			float Ee = sqrt(MPION*MPION + strawTrkFinal_[e].pz*strawTrkFinal_[e].pz + PeX*PeX + PeY*PeY);

			int tempMmissFlag = 0;

			for (int f = 0; f < nTrackFinal; f++) {
				if(f == e) continue;

				float PfX = strawTrkFinal_[f].pz * strawTrkFinal_[f].m1x;
				float PfY = strawTrkFinal_[f].pz * strawTrkFinal_[f].my;
				float Ef = sqrt(MPION*MPION + strawTrkFinal_[f].pz*strawTrkFinal_[f].pz + PfX*PfX + PfY*PfY);

				float EMiss = (EKaon - Ee - Ef);
				float PMissZ = (PKaonZ - strawTrkFinal_[e].pz - strawTrkFinal_[f].pz);
				float PMissX = (PKaonX - PeX - PfX);
				float PMissY = (PKaonY - PeY - PfY);
				float MMiss2 = EMiss*EMiss - PMissZ*PMissZ - PMissX*PMissX - PMissY*PMissY;

				if(MMiss2 > 15000 and MMiss2 < 25000) tempMmissFlag = 1;
			}

			if (tempMmissFlag == 0) flagL1Mmiss = 1;
		}

		if (flagL1Limit[e] > 0 && flagL1Three[e] == 0) {
			flagL1Pnn = 1;
		}
		if (flagL1Limit[e] > 0 && flagL1Three[e] == 0 && flagL1MUV3Tight[e]>0) {
			flagL1PnnMUV3Tight = 1;
		}
		if (strawTrkFinal_[e].zvertex > -100000 && strawTrkFinal_[e].zvertex < 180000 && strawTrkFinal_[e].cda < 200
				&& strawTrkFinal_[e].pz < 65000) {
			flagL1OneTrack = 1 ;
		}

	}

	///// exo part
	for (int e = 0; e < nTrackFinalExo; e++) {

		qTrack1.setPoint(0.0, strawTrkFinalExo_[e].q1x, strawTrkFinalExo_[e].qy, 0.0, 0.0, 0.0, 0.0, 0, 0);
		mTrack1.setPoint(1.0, strawTrkFinalExo_[e].m1x, strawTrkFinalExo_[e].my, 0.0, 0.0, 0.0, 0.0, 0, 0);

		for (int f = e + 1; f < nTrackFinalExo; f++) {
			qTrack2.setPoint(0.0, strawTrkFinalExo_[f].q1x, strawTrkFinalExo_[f].qy, 0.0, 0.0, 0.0, 0.0, 0, 0);
			mTrack2.setPoint(1.0, strawTrkFinalExo_[f].m1x, strawTrkFinalExo_[f].my, 0.0, 0.0, 0.0, 0.0, 0, 0);

			cdaVertex(qTrack1, qTrack2, mTrack1, mTrack2, cda, vertex);
			if (cda < 100) {
				if(((strawTrkFinalExo_[e].m1x - strawTrkFinalExo_[e].m2x) * (strawTrkFinalExo_[f].m1x - strawTrkFinalExo_[f].m2x)) < 0.) {

					if(vertex.z < 200000.) {
						pointToLineDistance(QBeam_, vertex, MBeam_,distance_temp);

						if (distance_temp > distanceToBeamLine) {
							distanceToBeamLine = distance_temp;
							l1Info->setL1StrawExo2TrkCDA(cda);
							l1Info->setL1StrawExo2TrkVtxToBeamDistance(distanceToBeamLine);
						}
					}
				}
			} 
		}
	}

	if(nTrackFinal >= 3){
		flagL1MultiTrk = 1;
	}

	if(distanceToBeamLine > 50.){
		flagL1DVloose = 1;
	}

	l1Info->setL1StrawNTracks(nTrackFinal);
	l1Info->setL1StrawProcessed();

	flagL1 = ((flagL1ExoticTight & 0x1) << 7) | ((flagL1PnnMUV3Tight & 0x1) << 6) | ((flagL1OneTrack & 0x1) << 5) | ((flagL1Mmiss & 0x1) << 4) | ((flagL1DVloose& 0x1) << 3) | ((flagL1MultiTrk & 0x1) << 2) | ((flagL1Exotic & 0x1) << 1) | (flagL1Pnn & 0x1);

	l1Info->setL1StrawTriggerWord(flagL1);
	delete[] addhit1;
	delete[] addhit2;

	return flagL1;
}

uint_fast8_t StrawAlgo::abortProcessing(L1InfoToStorage* l1Info) {
	l1Info->setL1StrawOverflow();
	l1Info->setL1StrawProcessed();
	l1Info->setL1StrawTriggerWord(0xff);

	// 3 for event fulfilled both L1 straw triggers for Kpnn and exotics data streams
	//return 3;
	/*
	 * Modification 28/04/2018
	 */
	// 0x3f for event fulfilled both all straw trigger flags
	return 0xff;
}

float StrawAlgo::posTubNew(int chamber, int view, int plane, int jstraw) {
	// Straw spacing = 17.6
	return 17.6 * jstraw + XOffset_[view][plane] + XOffCh_[chamber][view];
}

int StrawAlgo::strawAcceptance(int n, float v, float u, float x, float y, int zone) {

	float viewSize = 2100.0;
	float strawSpacing = 17.6;
	float strawInnerRadius = 4.875;
	float copperThickness = 0.00005;
	float mylarThickness = 0.036;
	float goldThickness = 0.00002;
	float delta = 0;
	float strawRadius = strawInnerRadius + 2 * copperThickness + mylarThickness + goldThickness;
	float strawDiameter = 2 * strawRadius;

	float viewPlaneTransverseSize = (120 - 1) * strawSpacing + strawDiameter; //circa 2104,22 -> meta: 1052,11

	int viewFlag[4] = { 0, 0, 0, 0 };

	float posView[4] = { v, u, x, y};
	float posAlongStraw[4] = { u, v, y, x};

	for (int jView = 0; jView < 4; jView++) {
		if (((posView[jView] > (HoleChamberMax_[n][jView] + delta) && posView[jView] < 0.5 * viewPlaneTransverseSize)
				|| (posView[jView] < (HoleChamberMin_[n][jView] - delta) && posView[jView] > -0.5 * viewPlaneTransverseSize))
				&& fabs(posAlongStraw[jView]) < 0.5 * viewSize) {
			viewFlag[jView] = 1;
		}
	}
	int Vv = viewFlag[0];
	int Vu = viewFlag[1];
	int Vx = viewFlag[2];
	int Vy = viewFlag[3];
	int nview = Vx + Vy + Vu + Vv;

// Zones
//  This code has been deactivated since we always requirestrawAcceptance
//	only two views
//
	switch (zone) {		
	case 0:  // Two views only
		if (Vv == 1 and Vu == 1 and Vx == 0 and Vy == 0)
			return 1;
		return 0;
	case 1:  // At least 1 view
		if (Vv == 1 and Vu == 0 and Vx == 1 and Vy == 0)
			return 1;
		return 0;
	case 2:  // At least 2 views
		if (Vv == 1 and Vu == 0 and Vx == 0 and Vy == 1)
			return 1;
		return 0;
	case 3:  // At least 3 views
		if (Vv == 0 and Vu == 1 and Vx == 1 and Vy == 0)
			return 1;
		return 0;
	case 4:  // Four views only
		if (Vv == 0 and Vu == 1 and Vx == 0 and Vy == 1)
			return 1;
		return 0;
	case 5:  // One view only
		if (Vv == 0 and Vu == 0 and Vx == 1 and Vy == 1)
			return 1;
		return 0;
	default:
		return 0;
	}
	if(zone == 12){ // One and Two views
	  if ( nview>0 and nview<3)
	    return 1;
		// if ( nview == 2)
		// 	return 2;
		// if ( nview == 1)
	 //    	return 1;
	}
	return 0;
}

void StrawAlgo::pointToLineDistance(const Point& qBeam, Point& vertex, const Point& mBeam, float& distance){

	Point p2;

	p2.x = mBeam.x + qBeam.x;
	p2.y = mBeam.y + qBeam.y;
	p2.z = mBeam.z + qBeam.z;

	Point a, b;

	a.x = vertex.x - qBeam.x;
	a.y = vertex.y - qBeam.y;
	a.z = vertex.z - ZMAGNET - qBeam.z;

	b.x = p2.x - qBeam.x;
	b.y = p2.y - qBeam.y;
	b.z = p2.z - qBeam.z;

	float cc, dd, ee;

	cc = a.x * b.x + a.y * b.y + a.z * b.z;
	dd = b.x * b.x + b.y * b.y + b.z * b.z;
	ee = cc/dd;

	Point f, g;

	f.x = ee * b.x;
	f.y = ee * b.y;
	f.z = ee * b.z;

	g.x = a.x - (ee * b.x);
	g.y = a.y - (ee * b.y);
	g.z = a.z - (ee * b.z);

	distance = sqrt(g.x * g.x + g.y * g.y + g.z * g.z);

}


void StrawAlgo::cdaVertex(const Point& qBeam, Point& qTrack, const Point& mBeam, Point& mTrack, float& cda, Point& vertex) {

	Point r12;
	float t1, t2, aa, bb, cc, dd, ee, det;
	Point q1, q2;

	r12.z = qBeam.z - qTrack.z;
	r12.x = qBeam.x - qTrack.x;
	r12.y = qBeam.y - qTrack.y;

	aa = mBeam.x * mBeam.x + mBeam.y * mBeam.y + mBeam.z * mBeam.z;
	bb = mTrack.x * mTrack.x + mTrack.y * mTrack.y + mTrack.z * mTrack.z;
	cc = mBeam.x * mTrack.x + mBeam.y * mTrack.y + mBeam.z * mTrack.z;
	dd = r12.x * mBeam.x + r12.y * mBeam.y + r12.z * mBeam.z;
	ee = r12.x * mTrack.x + r12.y * mTrack.y + r12.z * mTrack.z;
	det = cc * cc - aa * bb;

	t1 = (bb * dd - cc * ee) / det;
	t2 = (cc * dd - aa * ee) / det;

	q1.z = qBeam.z + t1 * mBeam.z;
	q1.x = qBeam.x + t1 * mBeam.x;
	q1.y = qBeam.y + t1 * mBeam.y;
	q2.z = qTrack.z + t2 * mTrack.z;
	q2.x = qTrack.x + t2 * mTrack.x;
	q2.y = qTrack.y + t2 * mTrack.y;

	vertex.setPoint(ZMAGNET + (q1.z + q2.z) / 2, (q1.x + q2.x) / 2, (q1.y + q2.y) / 2, 0.0, 0.0, 0.0, 0.0, 0, 0);
	r12.setPoint(q1.z - q2.z, q1.x - q2.x, q1.y - q2.y, 0.0, 0.0, 0.0, 0.0, 0, 0);

	cda = sqrt(r12.x * r12.x + r12.y * r12.y + r12.z * r12.z);

}

void StrawAlgo::writeData(L1StrawAlgo* algoPacket, uint l0MaskID, L1InfoToStorage* l1Info) {

	if (AlgoID_ != algoPacket->algoID)
		LOG_ERROR("Algo ID does not match with Algo ID written within the packet!");

	algoPacket->algoID = AlgoID_;
	algoPacket->onlineTimeWindow = (uint) AlgoOnlineTimeWindow_[l0MaskID];
	algoPacket->qualityFlags = (AlgoRefTimeSourceID_[l0MaskID] << 7) | (l1Info->isL1StrawProcessed() << 6) | (l1Info->isL1StrawEmptyPacket() << 4)
			| (l1Info->isL1StrawBadData() << 2) | (l1Info->isL1StrawOverflow() << 1) | ((uint) l1Info->getL1StrawTrgWrd(l0MaskID));

	for (uint iTrk = 0; iTrk != 4; iTrk++) {
		algoPacket->l1Data[iTrk] = (uint) l1Info->getL1StrawTrackVz(iTrk);
		algoPacket->l1Data[iTrk + 4] = (uint) l1Info->getL1StrawTrackP(iTrk);
	}
	algoPacket->l1Data[8] = (uint) l1Info->getL1StrawExo2TrkCDA(); // CDA 2-track vertex
	algoPacket->l1Data[9] = (uint) l1Info->getL1StrawExo2TrkVtxToBeamDistance(); // displaced vertex (max value)
	algoPacket->l1Data[10] = l1Info->getL1StrawNTracks();
	algoPacket->l1Data[11] = AlgoOnlinePNNMomentumCut_[l0MaskID];
	algoPacket->l1Data[12] = l1Info->getL1StrawTriggerWord();

	algoPacket->numberOfWords = (sizeof(L1StrawAlgo) / 4.);

}


} /* namespace na62 */

