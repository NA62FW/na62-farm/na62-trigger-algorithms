/*
 * RICHMultiTrackAlgo.h
 *
 *  Created on: 7 Aug 2017
 *      Author: romano
 *
 *  Modified on: 1 Oct 2021
 *      Author: angela romano
 *      Email: angela.romano@cern.ch
 */

#ifndef L1_RICHMULTITRACKALGO_H_
#define L1_RICHMULTITRACKALGO_H_

#include <sys/types.h>
#include <cstdint>

#ifndef ONLINEHLT
#include <common/decoding/DecoderHandler.h>
#else
#include <common/decoding/OfflineDecoderHandler.h>
#endif

#include <l1/L1InfoToStorage.h>
#include "rich_algorithm/ParsConfFile.h"
#include <struct/HLTConfParams.h>
#include <l1/L1Fragment.h>

#include "rich_algorithm/RICHChannelID.h"
#include <l1/StrawAlgo.h>

#define maxNhits 500

namespace na62 {
class RICHMultiTrackAlgo {
public:
	/**
	 * @param event Event* This is a pointer to the built Event containing all subevents (except those from the LKr)
	 *
	 * The event pointer may not be deleted by this method as it is deleted by the EventBuilder
	 *
	 * @return uint_fast8_t <0> if the event is rejected, the L1 trigger type word in other cases.
	 */
        static uint_fast8_t processRICHMultiTrackTrigger(uint l0MaskID, DecoderHandler& decoder, L1InfoToStorage* l1Info, StrawAlgo *runstraw);
	static void initialize(uint i, l1RICH &l1RICHStruct);
	static void loadConfigurationFile(std::string absolute_chMapFile_path, std::string absolute_pmPosFile_path, std::string absolute_mirrorFile_path);
	static void writeData(L1Algo* algoPacket, uint l0MaskID, L1InfoToStorage* l1Info);
	static float* getChPosFocalCorr(int diskID);

private:

	static RICHParsConfFile* InfoRICH_;
	static uint AlgoID_; //0 for CHOD, 1 for RICH, 2 for KTAG, 3 for LAV, 4 for IRCSAC, 5 for Straw, 6 for MUV3, 7 for NewCHOD
	static uint AlgoLogic_[16];
	static uint AlgoRefTimeSourceID_[16];
	static float AlgoOnlineTimeWindow_[16];

	static int* PMTGeo_;
	static float* PMTPos_;
	static float* t0Time;
	static int* focalCenterJura_;
	static int* focalCenterSaleve_;
	static float* extraOffsetJura_;
	static float* extraOffsetSaleve_;
	static float focalCorrection_[2];
	static float fitPositionX[maxNhits];
	static float fitPositionY[maxNhits];
	static float leadRecoTime[maxNhits];

};

} /* namespace na62 */

#endif /* L1_RICHMULTITRACKALGO_H_ */
