/*
 * RICHMultiTrackAlgo.cpp
 *
 *  Created on: 7 Aug 2017
 *      Author: angela romano
 *
 *  Modified on: 1 Oct 2021
 *      Author: angela romano
 *      Email: angela.romano@cern.ch
 */

#include "RICHMultiTrackAlgo.h"

#include <string.h>
#include <math.h>
#include <sys/time.h>

#ifndef ONLINEHLT
#include <l0/Subevent.h>
#else
#include <l0/offline/Subevent.h>
#endif
#include <options/Logging.h>

#include <common/decoding/TrbFragmentDecoder.h>
#include <l1/L1InfoToStorage.h>
#include <l1/StrawAlgo.h>
#include <l1/straw_algorithm/Track.h>

extern "C" {
   void initialize_rich() {
       l1RICH l1RICHStruct;
       l1RICHStruct.configParams.l1TrigProcessID = 1;
       l1RICHStruct.configParams.l1TrigMaskID = 0;
       l1RICHStruct.configParams.l1TrigEnable = 1;
       l1RICHStruct.configParams.l1TrigLogic = 1;
       l1RICHStruct.configParams.l1TrigFlag = 0;
       l1RICHStruct.configParams.l1TrigRefTimeSourceID = 0;
       l1RICHStruct.configParams.l1TrigOnlineTimeWindow = 5;

       na62::RICHMultiTrackAlgo::initialize(0, l1RICHStruct);
   }

   void load_rich_conf(std::vector<std::string>* vec_of_strings) {
     na62::RICHMultiTrackAlgo::loadConfigurationFile(vec_of_strings->at(0),
						     vec_of_strings->at(1),
						     vec_of_strings->at(2));
   }

   uint_fast8_t process_rich(uint l0MaskID, void* decoder_ptr, void* l1_info_ptr, void* straw_algo_ptr) {
     na62::DecoderHandler* decoded_event = reinterpret_cast<na62::DecoderHandler*>(decoder_ptr);
     L1InfoToStorage* l1_info = reinterpret_cast<L1InfoToStorage*>(l1_info_ptr);
     na62::StrawAlgo* straw_algo = reinterpret_cast<na62::StrawAlgo*>(straw_algo_ptr);
     return na62::RICHMultiTrackAlgo::processRICHMultiTrackTrigger(l0MaskID, *decoded_event, l1_info, straw_algo);
   }
}

namespace na62 {

uint RICHMultiTrackAlgo::AlgoID_; //0 for CHOD, 1 for RICH, 2 for KTAG, 3 for LAV, 4 for IRCSAC, 5 for Straw, 6 for MUV3, 7 for NewCHOD
uint RICHMultiTrackAlgo::AlgoLogic_[16];
uint RICHMultiTrackAlgo::AlgoRefTimeSourceID_[16];
float RICHMultiTrackAlgo::AlgoOnlineTimeWindow_[16];

RICHParsConfFile* RICHMultiTrackAlgo::InfoRICH_ = RICHParsConfFile::GetInstance();
int* RICHMultiTrackAlgo::PMTGeo_ = nullptr;
float* RICHMultiTrackAlgo::PMTPos_ = nullptr;
float* RICHMultiTrackAlgo::t0Time = nullptr;
int* RICHMultiTrackAlgo::focalCenterJura_ = nullptr;
int* RICHMultiTrackAlgo::focalCenterSaleve_ = nullptr;
float* RICHMultiTrackAlgo::extraOffsetJura_ = nullptr;
float* RICHMultiTrackAlgo::extraOffsetSaleve_ = nullptr;
float RICHMultiTrackAlgo::focalCorrection_[2];
float RICHMultiTrackAlgo::fitPositionX[maxNhits];
float RICHMultiTrackAlgo::fitPositionY[maxNhits];
float RICHMultiTrackAlgo::leadRecoTime[maxNhits];


void RICHMultiTrackAlgo::initialize(uint i, l1RICH &l1RICHStruct) {

	AlgoID_ = l1RICHStruct.configParams.l1TrigMaskID;
	AlgoLogic_[i] = l1RICHStruct.configParams.l1TrigLogic;
	AlgoRefTimeSourceID_[i] = l1RICHStruct.configParams.l1TrigRefTimeSourceID; //0 for L0TP, 1 for CHOD, 2 for RICH
	AlgoOnlineTimeWindow_[i] = l1RICHStruct.configParams.l1TrigOnlineTimeWindow;
}

  void RICHMultiTrackAlgo::loadConfigurationFile(std::string absolute_chMapFile_path, std::string absolute_pmPosFile_path, std::string absolute_mirrorFile_path) {
	InfoRICH_->loadConfigFile(absolute_chMapFile_path);
	InfoRICH_->readPosPmsMap(absolute_pmPosFile_path);
	InfoRICH_->readMirrorAlignment(absolute_mirrorFile_path);
	PMTGeo_ = InfoRICH_->getGeoPmsMap();
	PMTPos_ = InfoRICH_->getPosPmsMap();
	//t0Time = InfoRICH_->getT0();
	focalCenterJura_ = InfoRICH_->getFocalCenterJura();
	focalCenterSaleve_ = InfoRICH_->getFocalCenterSaleve();
	extraOffsetJura_ = InfoRICH_->getExtraOffsetJura();
	extraOffsetSaleve_ = InfoRICH_->getExtraOffsetSaleve();
}

  uint_fast8_t RICHMultiTrackAlgo::processRICHMultiTrackTrigger(uint l0MaskID, DecoderHandler& decoder, L1InfoToStorage* l1Info, StrawAlgo *runstraw) {

	using namespace l0;

	if(l1Info->isL1StrawOverflow()){
	  // no STRAW tracks due to overflow in STRAW. Pass by default.
	  l1Info->setL1RICHNElRings(0);
	  l1Info->setL1RICHProcessed();
	  return 0xFF; // pass with all bits
	} 

	float refTimeL0TP = 0.;
	float averageCHODHitTime = 0.;
	bool isCHODRefTime = false;
	if (!AlgoRefTimeSourceID_[l0MaskID]) {
		refTimeL0TP = decoder.getDecodedEvent()->getFinetime() * 0.097464731802;
//		LOG_INFO("L1 reference finetime from L0TP (ns) " << refTimeL0TP);
	} else if (AlgoRefTimeSourceID_[l0MaskID] == 1) {
		if (l1Info->isL1CHODProcessed()) {
			isCHODRefTime = 1;
			averageCHODHitTime = l1Info->getCHODAverageTime();
		} else
			LOG_ERROR("RICHMultiTrackAlgo.cpp: Not able to use averageCHODHitTime as Reference Time even if requested!");
	} else
		LOG_ERROR("L1 Reference Time Source ID not recognised !!");

	uint nHits = 0;
	uint nEdgesTot = 0;

	//TODO: chkmax need to be USED
	DecoderRange<TrbFragmentDecoder> x = decoder.getRICHDecoderRange();
	if (x.begin() == x.end()) {
		LOG_ERROR("RICH MultiTrack: Empty decoder range!");
		l1Info->setL1RICHBadData();
		return 0;
	}

	for (TrbFragmentDecoder* richPacket : decoder.getRICHDecoderRange()) {
//	LOG_INFO("First time check (inside iterator) " << time[1].tv_sec << " " << time[1].tv_usec);

		if (!richPacket->isReady() || richPacket->isBadFragment()) {
			LOG_ERROR("RICH: This looks like a Bad Packet!!!! ");
			l1Info->setL1RICHBadData();
			return 0;
		}

		/**
		 * Get Arrays with hit Info
		 */
		const uint64_t* const edgeTime = richPacket->getTimes();
		const uint_fast8_t* const edgeChID = richPacket->getChIDs();
		const bool* const edgeIsLeading = richPacket->getIsLeadings();
		const uint_fast8_t* const edgeTdcID = richPacket->getTdcIDs();
		const uint_fast16_t sourceSubID = richPacket->getSourceSubId();
		float time, dtL0TP, dtCHOD;
		uint chRO;

		uint numberOfEdgesOfCurrentBoard = richPacket->getNumberOfEdgesStored();

//		LOG_INFO("Tel62 ID " << richPacket->getFragmentNumber() << " - Number of Edges found " << numberOfEdgesOfCurrentBoard);

		for (uint iEdge = 0; iEdge != numberOfEdgesOfCurrentBoard; iEdge++) {
//			LOG_INFO("Edge " << iEdge << " ID " << edgeIsLeading[iEdge]);
//			LOG_INFO("Edge " << iEdge << " chID " << (uint) edgeChID[iEdge]);
//			LOG_INFO("Edge " << iEdge << " tdcID " << (uint) edgeTdcID[iEdge]);
//			LOG_INFO("Edge " << iEdge << " time " << std::hex << edgeTime[iEdge] << std::dec);


		       /**
			 * Process leading edges only
			 *
			 */
			if (edgeIsLeading[iEdge]) {
				time = (edgeTime[iEdge] - decoder.getDecodedEvent()->getTimestamp() * 256.) * 0.097464731802;
//				LOG_INFO("Edge time (ns) " << time);

				chRO = sourceSubID * 512 + edgeTdcID[iEdge] * 32 + edgeChID[iEdge];

				RICHChannelID channel(PMTGeo_[chRO]);
				int chSeqID = channel.getChannelSeqID();

				if ((chSeqID == -1) || (chSeqID > 1951)) continue; // eliminating supercells
				int newSeqID = chSeqID % 976;


				if (!isCHODRefTime) {
					dtL0TP = fabs(time - refTimeL0TP);
					dtCHOD = -1.0e+28;
				} else
					dtCHOD = fabs(time - averageCHODHitTime);
//				LOG_INFO("dtL0TP " << dtL0TP << " dtCHOD " << dtCHOD);

				if ((!isCHODRefTime && dtL0TP<5.) || (isCHODRefTime && dtCHOD<5.)) {
				  if(nHits<maxNhits){
				    getChPosFocalCorr(channel.getDiskID());
				    fitPositionX[nHits] = PMTPos_[newSeqID*2] - focalCorrection_[0];
				    fitPositionY[nHits] = PMTPos_[newSeqID*2 + 1] - focalCorrection_[1];
				    
				    //leadRecoTime[nHits] = time - t0Time[chSeqID];
				    leadRecoTime[nHits] = time;
				    nHits++;				
				  }
				  else{
				    // array size limit. Break.
				    LOG_ERROR("RICH MultiTrack: More than 500 hits in event.");
				    l1Info->setL1RICHBadData();
				    return 0;
				  }
				}
			}
		}
		//LOG_INFO("Number of edges of current board " << numberOfEdgesOfCurrentBoard);
		nEdgesTot += numberOfEdgesOfCurrentBoard;
	}

	if (!nEdgesTot)
		l1Info->setL1RICHEmptyPacket();

	//	LOG_INFO("nHits " << nHits);


	constexpr float FocalLength = 17020.;
	constexpr float ExpectedElectronRadius = 190.;
	uint ElectronCounter = 0;

	for (uint track_index = 0; track_index < l1Info->getL1StrawNTracks(); ++track_index) {

	  na62::Track &STRAWTrack = runstraw->getTracks(track_index);

	  //float Momentum = 0.001*STRAWTrack.pz; // GeV
	  float mx = STRAWTrack.m2x;
	  float my = STRAWTrack.my;
	  float xc = FocalLength * mx; // Ring center
	  float yc = FocalLength * my;

	  float Radius = 0.;
	  float RICHTime = 0.;
	  int NGoodHits = 0;
	  for (uint jHit = 0; jHit < nHits; jHit++) {
	    float xx = fitPositionX[jHit] - xc;
	    float yy = fitPositionY[jHit] - yc;
	    float Distance = sqrt(xx * xx + yy * yy);
	    float HitTime = leadRecoTime[jHit];

	    if (fabs(Distance - ExpectedElectronRadius) < 30.) {
	      Radius += Distance;
	      RICHTime += HitTime;
	      NGoodHits++;
	    }
	  }

	  if (NGoodHits>=3){
	    Radius = Radius / NGoodHits;
	    RICHTime = RICHTime / NGoodHits;
	    RICHTime = RICHTime - refTimeL0TP; // RICHTime defined wrt L0TriggerTime

	    if ((Radius-ExpectedElectronRadius) < 10. && (Radius - ExpectedElectronRadius) > -5. && fabs(RICHTime) < AlgoOnlineTimeWindow_[l0MaskID]) { // cut on ring
	      ElectronCounter++;
	    }
	  }
	}

	l1Info->setL1RICHNElRings(ElectronCounter);
	l1Info->setL1RICHProcessed();
	return (ElectronCounter >= 1);
}

float* RICHMultiTrackAlgo::getChPosFocalCorr(int diskID) {
	if (diskID == 0) {
	        focalCorrection_[0] = focalCenterJura_[0] + extraOffsetJura_[0];
	        focalCorrection_[1] = focalCenterJura_[1] + extraOffsetJura_[1];
	} else {
	        focalCorrection_[0] = focalCenterSaleve_[0] + extraOffsetSaleve_[0];
	        focalCorrection_[1] = focalCenterSaleve_[1] + extraOffsetSaleve_[1];
	}
	return focalCorrection_;
}


void RICHMultiTrackAlgo::writeData(L1Algo* algoPacket, uint l0MaskID, L1InfoToStorage* l1Info) {
	if (AlgoID_ != algoPacket->algoID) {
		LOG_ERROR("Algo ID does not match with Algo ID already written within the packet!");
        }

	algoPacket->algoID = AlgoID_;
	algoPacket->onlineTimeWindow = (uint) AlgoOnlineTimeWindow_[l0MaskID];
	algoPacket->qualityFlags = (AlgoRefTimeSourceID_[l0MaskID] << 7) | (l1Info->isL1RICHProcessed() << 6) | (l1Info->isL1RICHEmptyPacket() << 4) | (l1Info->isL1RICHBadData() << 2) | ((uint) l1Info->getL1RICHTrgWrd(l0MaskID));

	algoPacket->l1Data[0] = l1Info->getL1RICHNElRings();
	if (!AlgoRefTimeSourceID_[l0MaskID]) {
		algoPacket->l1Data[1] = l1Info->getL1RefTimeL0TP();
	} else if (AlgoRefTimeSourceID_[l0MaskID] == 1) {
		algoPacket->l1Data[1] = l1Info->getCHODAverageTime(); //this is a double!!!
	} else {
		LOG_ERROR("RICHMultiTrackAlgo.cpp: L1 Reference Time Source ID not recognised !!");
        }
        constexpr size_t number_of_words = sizeof(L1Algo) / 4;
	algoPacket->numberOfWords = number_of_words;
//	LOG_INFO("l0MaskID " << l0MaskID);
//	LOG_INFO("algoID " << (uint)algoPacket->algoID);
//	LOG_INFO("quality Flags " << (uint)algoPacket->qualityFlags);
//	LOG_INFO("online TW " << (uint)algoPacket->onlineTimeWindow);
//	LOG_INFO("Data Words " << algoPacket->l1Data[0] << " " << algoPacket->l1Data[1]);
}

} /* namespace na62 */
